QT       += core gui sql concurrent
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Podroznik
TEMPLATE = app
VERSION_APP = 0.1.1

DEFINES += APP_VERSION=\\\"$$VERSION_APP\\\"


APP_BASE = ../..
DESTDIR = $$APP_BASE/StellarTraveler


SOURCES += main.cpp\
        widget.cpp \
    database.cpp \
    world.cpp \
    trainer.cpp \
    preparationimage.cpp \
    recognitiontext.cpp \
    segmentationimage.cpp \
    correctiontext.cpp \
    appfolder.cpp \
    resolution.cpp \
    resolutionfactory.cpp

HEADERS  += widget.h \
    database.h \
    world.h \
    trainer.h \
    originalimage.h \
    preparationimage.h \
    recognitiontext.h \
    segmentationimage.h \
    correctiontext.h \
    appfolder.h \
    resolution.h \
    resolutionfactory.h

FORMS    += widget.ui \
    systems.ui


include(../../library.pri)

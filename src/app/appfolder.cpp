#include <QDir>
#include <QCoreApplication>
#include <QDebug>

const QString getDirApplication()
{
    QString appDirPath = qApp->applicationDirPath();
#ifdef Q_OS_MAC
    if (appDirPath.count("/")>3) {
        QString path = appDirPath + QLatin1String("/../..");
        QDir appDir = QDir(appDirPath);
        if (QFileInfo(QDir(path).absolutePath()).isBundle()) {
            appDir.cdUp();
            appDir.cdUp();
            appDir.cdUp();
            appDirPath = appDir.absolutePath();
        }
    }
#endif
    return appDirPath+"/";
}

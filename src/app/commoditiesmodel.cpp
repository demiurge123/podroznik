#include "commoditiesmodel.h"



CommoditiesModel::CommoditiesModel(QObject *parent) : QSqlTableModel(parent)
{
    query.prepare("SELECT name, sell, buy, demand, supply FROM Commodities;");
    setQuery(query);
}

CommoditiesModel::~CommoditiesModel()
{

}


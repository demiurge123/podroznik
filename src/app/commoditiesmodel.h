#pragma once
#include <QObject>
#include <QSqlTableModel>
#include <QSqlQuery>

class CommoditiesModel : public QSqlTableModel
{
    Q_OBJECT
public:
    explicit CommoditiesModel(QObject *parent = 0);
    ~CommoditiesModel();

signals:

public slots:
private:
    QSqlQuery query;
};


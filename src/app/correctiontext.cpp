#include <QDebug>
#include "correctiontext.h"
#include "database.h"

static QMutex mutexCorr;

QSharedPointer<QHash<QString, QString> > CorrectionText::stations(nullptr);
QSharedPointer<QHash<QString, QString> > CorrectionText::commodities(nullptr);
QSharedPointer<QHash<QString, QString> > CorrectionText::categories(nullptr);

CorrectionText::CorrectionText(const QStringList &data)
    : data(data)
{

}

void CorrectionText::save()
{


}



QSharedPointer<QHash<QString, QString>> prepareStationName(const Database* database)
{
    if( CorrectionText::stations.isNull() ) {
        CorrectionText::stations.reset( new QHash<QString, QString>() );

        QStringList listStations = database->getAllStationName();
        foreach(QString station,listStations) {
            QString shortStation = prepareToCompare(station.split("/").at(1));
            (*CorrectionText::stations)[shortStation]= station;
        }
    }
    return CorrectionText::stations;
}

QSharedPointer<QHash<QString, QString> > prepareCommodityName(const Database* database)
{
    if( CorrectionText::commodities.isNull() ) {
        CorrectionText::commodities.reset( new QHash<QString, QString>() );

        QStringList listCommodities = database->getAllCommodityName();
        foreach(QString commodity,listCommodities) {
            QString shortCommodity = prepareToCompare(commodity);
            (*CorrectionText::commodities)[shortCommodity]= commodity;
        }
    }
    return CorrectionText::commodities;
}

QSharedPointer<QHash<QString, QString> > prepareCategoryCommodityName(const Database* database)
{
    if( CorrectionText::categories.isNull() ) {
        CorrectionText::categories.reset( new QHash<QString, QString>() );

        QStringList listCategories = database->getAllCategoryName();
        foreach(QString category,listCategories) {
            QString shortCategory = prepareToCompare(category);
            (*CorrectionText::categories)[shortCategory]= category;
        }
    }
    return CorrectionText::categories;
}


QSharedPointer<CorrectionText> toCorrection(QSharedPointer<RecognitionText> recognition)
{
    QMutexLocker locker(&mutexCorr);
    //    QMap<int,QString> m_wordsOfCommodities, m_wordsOfStations, m_wordsOfCategoryCommodities;
    //    static QHash<QString, QString> m_commodities, m_stations, m_categories;
    QRegExp regexp("(?:(\\d{1,})|)((?:LOW|MED|HIGH))");
    QRegExp regexpCR("(\\d{1,})CR");
    
    QMap<int, QString> texts = recognition->getTexts();
    QMapIterator<int,QString> i(texts);
    QMap<int,QString> commodity;
    QStringList data;
    while(i.hasNext()) {
        i.next();
        QString text = i.value();
        if (i.key()==0) {
            QString stationName = matchingStationName(text);
            data.append(QString("@ ")+stationName);
        } else {
            QString name;
            switch((i.key()-1)%7) {
            case 0: {
                if (commodity.count()>0) {
                    QStringList d;
                    for(int j=0;j<7;j++) {
                        QString s = commodity.value(j,QString());
                        d<<s;
                    }
                    data.append(d.join(":"));
                    commodity.clear();
                }
                name = matchingCommodityName(text);
                if (name.isEmpty()) {
                    name = matchingCategoryCommodityName(text);
                    if (!name.isEmpty())
                        name=QString("+ ")+name;
                    else
                        name="?";
                }

                break;
            }

            case 1: // sell
            case 2: // buy
            {
                text.replace("O","0");
                text.replace("B","8");
                bool ok;
                text.toInt(&ok);
                if (ok)
                    name = text;
                else
                    name = QString("???"+text+"???");
                break;
            }

            case 3: // cargo
                name="";
                break;

            case 4: // demand
            case 5: // supply
            {
                if (text.indexOf(regexp)>-1) {
                    bool ok;
                    if (!regexp.cap(1).isEmpty()) {
                        regexp.cap(1).toInt(&ok);
                        if (ok)
                            name = regexp.cap(1);
                    }
                    if (regexp.cap(2).compare("LOW")==0)
                        name += "L";
                    else
                        if (regexp.cap(2).compare("MED")==0)
                            name += "M";
                        else
                            if (regexp.cap(2).compare("HIGH")==0)
                                name += "H";
                            else
                                name +="?";
                } else
                    name="?";
                break;
            }
            case 6: // avgPrice
            {
                if (text.indexOf(regexpCR)>-1) {
                    bool ok;
                    regexpCR.cap(1).toInt(&ok);
                    if (ok)
                        name=regexpCR.cap(1);
                    else
                        name="?";
                } else
                    name="?";
                break;
            }
            default:
                break;
            }
            commodity.insert((i.key()-1)%7,name);
        }
        if (!i.hasNext()) {
            if (commodity.count()>0) {
                QStringList d;
                for(int j=0;j<7;j++) {
                    QString s = commodity.value(j,QString());
                    d<<s;
                }
                data.append(d.join(":"));
                commodity.clear();
            }
        }
    }

    return QSharedPointer<CorrectionText>(new CorrectionText(data));
}



QString matchingStationName(const QString &nameStation) {
    QString shortStation = prepareToCompare(nameStation);
    if (shortStation.isEmpty() || CorrectionText::stations==nullptr)
        return QString();
    QString result = match(shortStation,CorrectionText::stations.data());
    if (!result.isEmpty())
        return CorrectionText::stations->value(result);
    return QString();
}

QString matchingCommodityName(const QString &nameCommodity) {
    QString shortCommodity = prepareToCompare(nameCommodity);
    if (shortCommodity.isEmpty() || CorrectionText::commodities == nullptr)
        return QString();
    QString result = match(shortCommodity,CorrectionText::commodities.data());
    if (!result.isEmpty())
        return CorrectionText::commodities->value(result);
    return QString();
}

QString matchingCategoryCommodityName(const QString &nameCategory) {
    QString shortCategory = prepareToCompare(nameCategory);
    if (shortCategory.isEmpty() || CorrectionText::categories == nullptr)
        return QString();
    QString result = match(shortCategory,CorrectionText::categories.data());
    if (!result.isEmpty())
        return CorrectionText::categories->value(result);
    return QString();
}

int searchMatchedLetters(QString A, QString B) {
    if (A.length()<B.length())
        A.swap(B);
    if (A.indexOf(B)>-1)
        return A.length()-A.indexOf(B);
    if (A.length()==1 || B.length()==1)
        return 0;

    int matchingLetters=0;
    if (A.length()%2==1 && B.length()%2==1)
        matchingLetters=A.at(A.length()/2)==B.at(B.length()/2)?1:0;
    matchingLetters+=searchMatchedLetters(A.left(A.length()/2),B.left(B.length()/2+1));
    matchingLetters+=searchMatchedLetters(A.right(A.length()/2),B.right(B.length()/2));

    return matchingLetters;
}

const QList<QString> getListOfPossibleMatches(int numberOfLetters, const QHash<QString, QString> *dictionary)
{
    QList<QString> list;

    QHashIterator<QString, QString> iter(*dictionary);
    while (iter.hasNext()) {
        iter.next();
        QString shortWord =iter.key();
        if (std::abs(shortWord.length() - numberOfLetters)<=2)
            list.append(shortWord);
    }
    return list;
}

QString match(const QString &name, const QHash<QString, QString> *dictionary)
{
    int numberOfLetters = name.size();
    if (dictionary == nullptr)
        return QString();
    
    QList<QString> list = getListOfPossibleMatches(numberOfLetters, dictionary);

    if (list.size()==0)
        return QString();
    
    int index;
    if ((index=list.indexOf(name))>-1)
        return list.at(index);
    
    QScopedArrayPointer<int> match(new int[list.size()]);
    
    for (int i=0;i<list.size();i++) {
        match[i]=searchMatchedLetters(list.at(i),name);
    }
    
    int bestMatchLetters=0;
    index=-1;
    QString result;
    for (int i=0;i<list.size();i++)
        if (bestMatchLetters<match[i]) {
            bestMatchLetters=match[i];
            index=i;
        }
    if (index>-1)
        result = list.at(index);
    
    bool goodLevelOfMatchingLetters = ( bestMatchLetters >= (0.7 * numberOfLetters) );
    if (goodLevelOfMatchingLetters)
        return result;
    
    return QString();
}


QString prepareToCompare(QString text)
{
    if (text.isEmpty())
        return QString();
    QString result = text.remove(QRegExp("[ -.]"));
    result = result.toUpper();
    return result;
}

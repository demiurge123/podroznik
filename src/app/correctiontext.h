#pragma once
#include "database.h"
#include "recognitiontext.h"



class CorrectionText
{
private:
    QStringList data;
public:
    static QSharedPointer<QHash<QString, QString> > stations;
    static QSharedPointer<QHash<QString, QString> > commodities;
    static QSharedPointer<QHash<QString, QString> > categories;

    CorrectionText(const QStringList& data=QStringList());
    void save();
    const QStringList getList() const { return data; }
};

QSharedPointer<CorrectionText> toCorrection(QSharedPointer<RecognitionText> recognition);



QSharedPointer<QHash<QString, QString>> prepareStationName(const Database *database);
QSharedPointer<QHash<QString, QString>> prepareCommodityName(const Database *database);
QSharedPointer<QHash<QString, QString>> prepareCategoryCommodityName(const Database *database);

QString matchingStationName(const QString &nameStation);
QString matchingCommodityName(const QString &nameCommodity);
QString matchingCategoryCommodityName(const QString &nameCategory);
QString match(const QString &name, const QHash<QString, QString> *dictionary);
QString prepareToCompare(QString text);

#include "database.h"
#include <QFile>
#include <QCoreApplication>
#include <QDir>
#include <QSqlQuery>
#include <QSqlError>
#include <QMutex>
#include <QTextStream>
#include <QDateTime>
#include <QVariant>
#include <cmath>
#include <QDebug>
#include <tuple>
#include <map>
#include "appfolder.h"

void Database::setData(QSharedPointer<MarketData> data)
{

}

QStringList Database::getAllCommodityName() const
{
    QStringList listNameCommodities;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);
    
    query.exec("SELECT name FROM NameCommodity;");
    while(query.next()) {
        QString name = query.value("name").toString();
        listNameCommodities.append(name);
    }
    return listNameCommodities;
}

QStringList Database::getAllCategoryName() const
{
    QStringList listNameCategories;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);
    
    query.exec("SELECT name FROM CategoryCommodity;");
    while(query.next()) {
        QString name = query.value("name").toString();
        listNameCategories.append(name);
    }
    return listNameCategories;
}

void Database::createStation(const QString &nameStation, QString &nameSystem)
{
    if (nameStation.isEmpty() || nameSystem.isEmpty())
        return;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);

    bool addedStation=false, addedStationInSystem=false;
    if (db.transaction()) {
        query.prepare("INSERT INTO NameStation(name) VALUES(?);");
        query.addBindValue(nameStation);
        addedStation = query.exec();

        query.prepare("INSERT INTO Station(name,system) VALUES(?,?);");
        query.addBindValue(nameStation);
        query.addBindValue(nameSystem);
        addedStationInSystem = query.exec();

        if (addedStation && addedStationInSystem)
            db.commit();
        else
            db.rollback();
    }
}

QStringList Database::getAllStationName() const
{
    QStringList listNameStations;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);
    
    query.exec("SELECT name, system FROM Station;");
    while(query.next()) {
        QString name = query.value("name").toString();
        QString system = query.value("system").toString();
        listNameStations.append(QString("%1/%2").arg(system).arg(name));
    }
    return listNameStations;
}

QStringList Database::getAllSystemName() const
{
    QStringList systemList;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);

    query.exec("SELECT name FROM System;");
    while(query.next()) {
        QString name = query.value("name").toString();
        systemList.append(name);
    }
    return systemList;
}

Database::Database(const QString databaseName)
    :databaseName(databaseName)
{
    QDir dir = QDir(getDirApplication());
    appPath = dir.absolutePath();
    bool existingData = QFile::exists(appPath+"/"+databaseName);
    if (createConnection()&&!existingData) {
        createData();
        addSystems();
        addStations();
        createDistances();
    }
}

Database::~Database()
{
    QSqlDatabase::removeDatabase(databaseName);
}

bool Database::createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(appPath+"/"+databaseName);


    if (!db.open()) {
        qDebug()<<"Error connection with database: " << db.lastError().text();
        //        QLogger::QLog_Warning("Database", "Error connection with database: " + m_db.lastError().text());
        return false;
    }
    return true;
}

void Database::createData()
{
    QSqlQuery query;
    QSqlDatabase db = QSqlDatabase::database();
    if (db.transaction()) {

        query.exec("CREATE TABLE 'Added' ('name' TEXT PRIMARY KEY UNIQUE);");
        query.exec("CREATE TABLE 'CategoryCommodity' ('name' TEXT PRIMARY KEY UNIQUE);");
        query.exec("CREATE TABLE 'Commodities' ('name' TEXT REFERENCES 'NameCommodity' ('name'), 'category' TEXT REFERENCES 'CategoryCommodity' ('name'));");
        query.exec("CREATE TABLE 'ConnectionStation' ('fromSystem' TEXT NOT NULL REFERENCES 'System' ('name'), 'fromStation' TEXT NOT NULL REFERENCES 'NameStation' ('name'), 'toSystem' TEXT NOT NULL REFERENCES 'System' ('name'), 'toStation' TEXT NOT NULL REFERENCES 'NameStation' ('name'), 'distance' REAL);");
        query.exec("CREATE TABLE 'LevelProduction' ('name' TEXT PRIMARY KEY UNIQUE);");
        query.exec("CREATE TABLE 'NameCommodity' ('name' TEXT PRIMARY KEY UNIQUE);");
        query.exec("CREATE TABLE 'NameStation' ('name' TEXT PRIMARY KEY UNIQUE);");
        query.exec("CREATE TABLE 'Station' ('name' TEXT REFERENCES 'NameStation' ('name'), 'system' TEXT REFERENCES 'System' ('name'), 'distance' REAL);");
        query.exec("CREATE TABLE 'StationBuying' ('station' TEXT REFERENCES 'NameStation' ('name'), 'system' TEXT REFERENCES 'System' ('name'), 'commodity' TEXT REFERENCES 'NameCommodity' ('name'), 'price' INTEGER, 'units' INTEGER, 'level' TEXT REFERENCES 'LevelProduction' ('name'), 'modified' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);");
        query.exec("CREATE TABLE 'StationSelling' ('station' TEXT REFERENCES 'NameStation' ('name'), 'system' TEXT REFERENCES 'System' ('name'), 'commodity' TEXT REFERENCES 'NameCommodity' ('name'), 'price' INTEGER, 'units' INTEGER, 'level' TEXT REFERENCES 'LevelProduction' ('name'), 'modified' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);");
        query.exec("CREATE TABLE 'System' ('name' TEXT PRIMARY KEY UNIQUE, 'x' REAL, 'y' REAL, 'z' REAL, 'added' TEXT REFERENCES 'Added' ('name'), 'modified' DATETIME);");

        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Alpha1' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Alpha2' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Alpha3' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Alpha4' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta1 (unverified)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta1 (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta1' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta1-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2 (outside Beta3)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2 (outside Beta3)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2 (unverified)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2 (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta2-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta3 (unverified)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta3 (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta3' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beta3-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Beyond The Pill (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma (unverified)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma1 (unverified)' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma1 (unverified)-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma1' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Gamma1-Inferred' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Not Present' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Premium Beta1' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'Premium Beta2' );");
        query.exec("INSERT INTO 'Added' ( 'name' ) VALUES ( 'undefined-Inferred' );");

        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Chemicals' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Consumer Items' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Foods' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Industrial Materials' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Legal Drugs' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Machinery' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Medicines' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Metals' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Minerals' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Technology' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Textiles' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Waste' );");
        query.exec("INSERT INTO 'CategoryCommodity' ( 'name' ) VALUES ( 'Weapons' );");

        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Advanced Catalysers' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Agri-Medicines' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Algae' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Alloys' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Aluminium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Animal Meat' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Animal Monitors' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Aquaponic Systems' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Atmospheric Processors' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Auto-Fabricators' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Basic Medicines' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Battle Weapons' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Bauxite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Beer' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Bertrandite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Beryllium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Bioreducing Lichen' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Biowaste' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Chemical Waste' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Clothing' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Cobalt' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Coffee' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Coltan' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Combat Stabilisers' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Computer Components' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Consumer Technology' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Copper' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Cotton' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Crop Harvesters' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Domestic Appliances' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Explosives' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Fish' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Food Cartridges' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Fruit and Vegetables' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Gallite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Gallium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Gold' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Grain' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'H.E. Suits' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Hydrogen Fuel' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Indite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Indium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Land Enrichment Systems' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Leather' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Lepidolite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Liquor' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Lithium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Marine Equipment' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Microbial Furnaces' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Mineral Extractors' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Mineral Oil' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Narcotics' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Natural Fabrics' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Non-Lethal Weapons' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Palladium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Performance Enhancers' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Personal Weapons' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Pesticides' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Plastics' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Platinum' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Polymers' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Power Generators' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Progenitor Cells' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Reactive Armour' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Resonating Separators' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Robotics' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Rutile' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Scrap' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Semiconductors' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Silver' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Superconductors' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Synthetic Fabrics' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Synthetic Meat' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Tantalum' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Tea' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Titanium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Tobacco' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Uraninite' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Uranium' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Water Purifiers' );");
        query.exec("INSERT INTO 'NameCommodity' ( 'name' ) VALUES ( 'Wine' );");

        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Explosives','Chemicals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Hydrogen Fuel','Chemicals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Mineral Oil','Chemicals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Pesticides','Chemicals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Clothing','Consumer Items' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Consumer Technology','Consumer Items' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Dom. Appliances','Consumer Items' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Algae','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Animal Meat','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Coffee','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Fish','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Food Cartridges','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Fruit and Vegetables','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Grain','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Synthetic Meat','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Tea','Foods' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Alloys','Industrial Materials' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Plastics','Industrial Materials' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Polymers','Industrial Materials' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Semiconductors','Industrial Materials' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Superconductors','Industrial Materials' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Beer','Legal Drugs' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Liquor','Legal Drugs' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Narcotics','Legal Drugs' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Tobacco','Legal Drugs' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Wine','Legal Drugs' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Atmospheric Processors','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Crop Harvesters','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Marine Equipment','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Microbial Furnaces','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Mineral Extractors','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Power Generators','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Water Purifiers','Machinery' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Agri-Medicines','Medicines' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Basic Medicines','Medicines' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Combat Stabilisers','Medicines' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Performance Enhancers','Medicines' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Progenitor Cells','Medicines' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Aluminium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Beryllium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Cobalt','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Copper','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Gallium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Gold','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Indium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Lithium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Palladium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Platinum','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Silver','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Tantalum','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Titanium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Uranium','Metals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Bauxite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Bertrandite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Coltan','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Gallite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Indite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Lepidolite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Rutile','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Uraninite','Minerals' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Advanced Catalysers','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Animal Monitors','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Aquaponic Systems','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Auto-Fabricators','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Bioreducing Lichen','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Computer Components','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'H.E. Suits','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Land Enrichment Systems','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Resonating Separators','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Robotics','Technology' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Cotton','Textiles' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Leather','Textiles' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Natural Fabrics','Textiles' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Synthetic Fabrics','Textiles' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Biowaste','Waste' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Chemical Waste','Waste' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Scrap','Waste' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Battle Weapons','Weapons' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Non-Lethal Wpns','Weapons' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Personal Weapons','Weapons' );");
        query.exec("INSERT INTO 'Commodities' ( 'name','category' ) VALUES ( 'Reactive Armour','Weapons' );");

        query.exec("INSERT INTO 'LevelProduction' ( 'name' ) VALUES ( 'LOW' );");
        query.exec("INSERT INTO 'LevelProduction' ( 'name' ) VALUES ( 'MED' );");
        query.exec("INSERT INTO 'LevelProduction' ( 'name' ) VALUES ( 'HIGH' );");

        query.exec("CREATE TABLE 'StationItem' ('station' TEXT REFERENCES 'NameStation' ('name'), 'system' TEXT REFERENCES 'System' ('name'), 'commodity' TEXT REFERENCES 'NameCommodity' ('name'), 'modified' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)");
        query.exec("CREATE VIEW vPrices AS SELECT si.station AS station, si.system AS system,si.commodity AS goods,IFNULL(sb.price, 0) AS sell, IFNULL(ss.price, 0) AS buy, si.modified AS modified, IFNULL(sb.units, 0) AS demand, IFNULL(sb.level, 0) AS 'lvl demand', IFNULL(ss.units, 0) AS supply, IFNULL(ss.level, 0) AS 'lvl supply' FROM  StationItem AS si LEFT OUTER JOIN StationBuying AS sb USING (commodity, station, system) LEFT OUTER JOIN StationSelling AS ss USING (station, system, commodity)");

        db.commit();
    }
}

void Database::addSystems()
{
    QSqlQuery query;
    QSqlDatabase db = QSqlDatabase::database();
    bool firstLine=true;
    QFile file(appPath+"/System.csv");
    if (file.open(QIODevice::Text|QIODevice::ReadOnly)) {
        QSqlDatabase db = QSqlDatabase::database();
        if (db.transaction()) {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                if (line.isEmpty())
                    break;
                QStringList data = line.split(",");
                if (firstLine) {
                    firstLine=false;
                    continue;
                }
                Q_ASSERT_X(data.size()==6,"addSystems",QString("Bad size data with line %1").arg(line).toStdString().c_str());
                if (data.size()==6) {
                    query.prepare("INSERT INTO System(name, x, y, z, added, modified) VALUES(?,?,?,?,?,?);");
                    QString name=data.at(0);
                    name.remove(QRegExp("^'"));
                    name.remove(QRegExp("'$"));
                    name.replace("''","'");
                    query.addBindValue(name.toUpper());
                    query.addBindValue(data.at(1).toDouble());
                    query.addBindValue(data.at(2).toDouble());
                    query.addBindValue(data.at(3).toDouble());
                    QString version = data.at(4);
                    version.remove(QRegExp("^'"));
                    version.remove(QRegExp("'$"));
                    query.addBindValue(version);
                    QString date=data.at(5);
                    date.remove(QRegExp("^'"));
                    date.remove(QRegExp("'$"));
                    query.addBindValue(QDateTime::fromString(date,"yyyy-MM-dd hh:mm:ss"));
                    if (!query.exec())
                        qDebug()<<name<<data.at(1).toDouble()<<data.at(2).toDouble()<<data.at(3).toDouble()<<version<<date;
                }

            }
            db.commit();
        }
    }
}

QString ucfirst(const QString str) {
    if (str.size() < 1) {
        return "";
    }

    QStringList tokens = str.split(" ");
    QList<QString>::iterator tokItr = tokens.begin();

    for (tokItr = tokens.begin(); tokItr != tokens.end(); ++tokItr) {
        (*tokItr) = (*tokItr).at(0).toUpper() + (*tokItr).mid(1);
    }

    return tokens.join(" ");
}

void Database::addStations()
{
    QSqlQuery query;
    QSqlDatabase db = QSqlDatabase::database();
    bool firstLine=true;
    QFile file(appPath+"/Station.csv");
    QStringList alreadyAddedStations;
    if (file.open(QIODevice::Text|QIODevice::ReadOnly)) {
        QSqlDatabase db = QSqlDatabase::database();
        if (db.transaction()) {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                if (line.isEmpty())
                    break;
                QStringList data = line.split(",");
                if (firstLine) {
                    firstLine=false;
                    continue;
                }
                if (data.size()==3) {

                    QString name=data.at(1);
                    name.remove(QRegExp("^'"));
                    name.remove(QRegExp("'$"));
                    name.replace("''","'");
                    name = ucfirst(name);

                    query.prepare("INSERT INTO NameStation(name) VALUES(?);");
                    query.addBindValue(name);
                    if (alreadyAddedStations.contains(name,Qt::CaseInsensitive))
                        qDebug()<<name;
                    alreadyAddedStations.append(name);
                    query.exec();

                    query.prepare("INSERT INTO Station(name, system, distance) VALUES(?,?,?);");
                    query.addBindValue(name);

                    QString system=data.at(0);
                    system.remove(QRegExp("^'"));
                    system.remove(QRegExp("'$"));
                    system.replace("''","'");
                    system = system.toUpper();
                    query.addBindValue(system);

                    query.addBindValue(data.at(2));

                    if (!query.exec())
                        qDebug()<<system<<name;
                }
            }
            db.commit();
        }
    }
}

void Database::createDistances()
{
    QSqlQuery query;
    QSqlDatabase db = QSqlDatabase::database();
    if (db.transaction()) {
        QStringList stations;
        query.exec("SELECT name, system FROM Station;");
        while (query.next()) {
            QString station=query.value("name").toString();
            QString system=query.value("system").toString();
            Q_ASSERT_X(!station.isEmpty(),"createDistances",QString("empty station name in %1").arg(system).toStdString().c_str());
            Q_ASSERT_X(!system.isEmpty(),"createDistances",QString("empty system name with %1").arg(station).toStdString().c_str());
            stations.append(QString("%1@%2").arg(station).arg(system));
        }
        std::map<int, std::tuple<double,double,double>> positions;
        for(int i=0;i<stations.size();i++) {
            QString system = stations.at(i).split("@").at(1).toUpper();
            Q_ASSERT_X(!system.isEmpty(),"createDistances",QString("empty system name in station %1").arg(stations.at(i)).toStdString().c_str());
            query.prepare("SELECT x,y,z FROM System WHERE name=?;");

            query.addBindValue(system);
            if (!query.exec())
                qDebug()<<query.lastError().text();
            if (query.next()) {
                double x,y,z;
                x=query.value("x").toDouble();
                y=query.value("y").toDouble();
                z=query.value("z").toDouble();
                positions.insert(std::make_pair(i, std::make_tuple(x,y,z)));
            } else {
                qDebug()<<system;
            }
        }
        Q_ASSERT(positions.size()==stations.size());
        for(int i=0;i<stations.size();i++)
            for (int j=i+1;j<stations.size();j++) {
                double x1,x2,y1,y2,z1,z2;
                std::tie(x1, y1, z1) = positions.at(i);
                std::tie(x2, y2, z2) = positions.at(j);
                double distance = std::sqrt(std::pow(x1-x2,2.0)+std::pow(y1-y2,2.0)+std::pow(z1-z2,2.0));
                QString nameStation1 = stations.at(i).split("@").at(0);
                QString nameSystem1 = stations.at(i).split("@").at(1);
                QString nameStation2 = stations.at(j).split("@").at(0);
                QString nameSystem2 = stations.at(j).split("@").at(1);

                query.prepare("INSERT INTO ConnectionStation(fromSystem, fromStation, toSystem, toStation, distance) VALUES(?,?,?,?,?);");
                query.addBindValue(nameSystem1);
                query.addBindValue(nameStation1);
                query.addBindValue(nameSystem2);
                query.addBindValue(nameStation2);
                query.addBindValue(distance);
                query.exec();

                query.prepare("INSERT INTO ConnectionStation(fromSystem, fromStation, toSystem, toStation, distance) VALUES(?,?,?,?,?);");
                query.addBindValue(nameSystem2);
                query.addBindValue(nameStation2);
                query.addBindValue(nameSystem1);
                query.addBindValue(nameStation1);
                query.addBindValue(distance);
                query.exec();
            }



        db.commit();
    }
}


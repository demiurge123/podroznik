#pragma once
#include <QSqlDatabase>
#include <QSharedPointer>
#include "marketdata.h"

class Database
{
public:
    Database(const QString databaseName);
    virtual ~Database();
    QSqlDatabase db() { return QSqlDatabase::database();}
    void setData(QSharedPointer<MarketData> data);
    QStringList getAllStationName() const;
    QStringList getAllSystemName() const;
    QStringList getAllCommodityName() const;
    QStringList getAllCategoryName() const;
    void createStation(const QString& nameStation, QString& nameSystem);
private:


    bool createConnection();
    void createData();
    void addSystems();
    void addStations();
    void createDistances();

    QString appPath;
    QString databaseName;
};



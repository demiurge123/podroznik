#pragma once
#include <QString>
#include <QDateTime>
#include <vector>

enum Level{None,Low,Med,High};

struct Goods {
    QString name;
    int sell;
    int buy;
    int demand;
    Level levelDemand;
    int supply;
    Level levelSupply;
    int avgPrice;
    Goods() {
        clear();
    }
    Goods(const Goods& goods) {
        name=QString(goods.name);
        sell=goods.sell;
        buy=goods.buy;
        demand=goods.demand;
        levelDemand=goods.levelDemand;
        supply=goods.supply;
        levelSupply=goods.levelSupply;
        avgPrice=goods.avgPrice;
    }
    void clear() {
        name.clear();
        sell=-1;
        buy=-1;
        demand=-1;
        levelDemand=None;
        supply=-1;
        levelSupply=None;
        avgPrice=-1;
    }
};

struct MarketData {
    QString nameMarket;
    std::vector<Goods> goods;
    QDateTime modified;
};

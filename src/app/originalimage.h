#pragma once
#include <QImage>
#include <QSharedPointer>

class OriginalImage
{
private:
    QImage image;
    QString path;
public:
    OriginalImage(QImage image=QImage(), QString path=QString())
        : image(image), path(path) {}
    int height() const { return image.height(); }
    int width() const { return image.width(); }
    const uchar* bits() const { return image.bits(); }
    int bytesPerLine() const { return image.bytesPerLine(); }
    const QString getPath() const { return path; }
};

#include <QSharedPointer>
#include <QMutex>
#include "preparationimage.h"
#include "resolutionfactory.h"
static QMutex mutexPrep;

QSharedPointer<PreparationImage> toPreparation(QSharedPointer<OriginalImage> original)
{
    QMutexLocker locker(&mutexPrep);
    cv::Mat src(original->height(),original->width(),CV_8UC4,(uchar*)original->bits(),original->bytesPerLine());

    curve(src,63,255);
    cv::cvtColor(src, src,cv::COLOR_RGBA2GRAY);

    boostLevel(src,10,51);

    ResolutionFactoryImpl factory;
    QSharedPointer<Resolution> resolution = factory.make(original->width(),original->height());

    clear(src,resolution->tl(),resolution->br());

    return QSharedPointer<PreparationImage>(new PreparationImage(src,original->getPath()));
}

void curve(cv::Mat &image, int indentLeft, int indentRight)
{
    cv::Mat target(image.cols,image.rows,image.type());

    double alpha=256.0/(indentRight - indentLeft); // 3.0
    int beta = -(indentLeft* alpha);  //-256
    image.convertTo(target, -1, alpha, beta);

    target.copyTo(image);
}

void clear(cv::Mat& image, const cv::Point& tl, const cv::Point& br)
{
    for (int i=tl.y;i<=br.y;i++) {
        int sum=0;
        for (int j=tl.x;j<=br.x;j++)
            sum+=image.at<uchar>(i,j);
        sum=sum/(br.x-tl.x+1);
        if (sum>102)
            for (int j=tl.x;j<=br.x;j++) {
                if (image.at<uchar>(i,j)<102)
                    image.at<uchar>(i,j)=255-image.at<uchar>(i,j);
                else
                    image.at<uchar>(i,j)=0;
            }
    }
}

void boostLevel(cv::Mat& src, int low, int high)
{

    for(int j=0;j<src.rows;j++)
    {
        for (int i=0;i<src.cols;i++)
        {
            uchar value=src.at<uchar>(j,i);
            if (value>=high)
                value=255;
            else {
                if (value<=low)
                    value=0;
                else
                    value-=low;
                value*=255.0/(high-low);
            }
            src.at<uchar>(j,i)=value;
        }
    }
}

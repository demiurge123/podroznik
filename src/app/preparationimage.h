#pragma once
#include <opencv2/opencv.hpp>
#include <QFileInfo>
#include "originalimage.h"

class PreparationImage
{
private:
    cv::Mat image;
    QString path;
public:
    PreparationImage(cv::Mat src=cv::Mat(), const QString& path=QString())
        :path(path) { src.copyTo(image); }
    void save() {
        cv::Mat src;
        cv::cvtColor(image, src,cv::COLOR_GRAY2RGBA);
        QImage img((uchar*) src.data, src.cols, src.rows, (int)src.step, QImage::Format_RGB32);
        QFile file(path);
        QFileInfo fileInfo(file);
        img.save(fileInfo.absolutePath()+"/_"+fileInfo.baseName()+".png");
    }
    cv::Mat getImage() const { return image; }
    QString getPath() const { return path; }
    int height() const { return image.rows; }
    int width() const { return image.cols; }
};

QSharedPointer<PreparationImage> toPreparation(QSharedPointer<OriginalImage> original);
void curve(cv::Mat &image, int indentLeft, int indentRight);
void clear(cv::Mat &image, const cv::Point& tl, const cv::Point& br);
void boostLevel(cv::Mat& src, int low, int high);


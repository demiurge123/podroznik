#include "recognitiontext.h"
#include "appfolder.h"
#include <QMutex>

static QMutex mutexReco;

void RecognitionText::save()
{
    QFile file(path);
    QFileInfo fileInfo(file);
    QFile output(fileInfo.absolutePath()+"/"+fileInfo.baseName()+".txt");
    if (output.open(QIODevice::WriteOnly|QIODevice::Text)) {
        QTextStream out(&output);
        QMapIterator<int, QString> i(texts);
        while(i.hasNext()) {
            i.next();
            if (i.key()==0)
                out<<i.value();
            else {
                if ((i.key()-1)%7==0)
                    out<<endl;
                out<<i.value()<<"\t";
            }
        }
        output.close();
    }
}

QSharedPointer<RecognitionText> toRecognition(QSharedPointer<SegmentationImage> segmentation)
{
    QMutexLocker locker(&mutexReco);
    QMap<int, QString> texts;
    QMap<int, cv::Mat> images = segmentation->getImages();

    int sizeLetters = 15;
    QScopedPointer<Trainer> m_trainer(new Trainer(sizeLetters));

    QMapIterator<int, cv::Mat> i(images);
    while(i.hasNext()) {
        i.next();
        cv::Mat src =  i.value();
        if (sizeLetters!=src.rows) {
            sizeLetters = src.rows;
            m_trainer.reset(new Trainer(sizeLetters));
        }

        QString text=m_trainer->recognitionImage(src);
        texts.insert(i.key(),text);
    }

    return QSharedPointer<RecognitionText>(new RecognitionText(texts,segmentation->getPath()));
}

#pragma once
#include <QMap>
#include <QCoreApplication>
#include <QDir>
#include <QTextStream>
#include "segmentationimage.h"
#include "trainer.h"

typedef QMap< int, QString> mapTexts;
Q_DECLARE_METATYPE(mapTexts)

class RecognitionText
{
private:
    mapTexts texts;
    QString path;
public:
    RecognitionText(mapTexts texts = mapTexts(), const QString& path=QString())
        : texts(texts), path(path) { }
    virtual ~RecognitionText() { texts.clear();}
    void save();
    const mapTexts getTexts() const {return texts;}
    QString getPath() const { return path; }
};

QSharedPointer<RecognitionText> toRecognition(QSharedPointer<SegmentationImage> segmentation);

#include "resolution.h"

Resolution2560_1440::Resolution2560_1440()
{
    width = 2560;
    height = 1440;
}

cv::Point Resolution2560_1440::tl() const
{
    return cv::Point(106,325);
}

cv::Point Resolution2560_1440::br() const
{
    return cv::Point(1714,1300);
}

std::vector<int> Resolution2560_1440::columns() const
{
    std::vector<int> cols {0, 486, 603, 719, 843, 1088, 1357};
    return cols;
}

QPair<int, int> Resolution2560_1440::lengthVHlines() const
{
    return QPair<int,int>(1600,900);
}

cv::Rect Resolution2560_1440::stationRect() const
{
    return cv::Rect(cv::Point(105,89),cv::Size(1605,38));
}

Resolution1680_1050::Resolution1680_1050()
{
    width = 1680;
    height = 1050;
}

cv::Point Resolution1680_1050::tl() const
{
    return cv::Point(70,266);
}

cv::Point Resolution1680_1050::br() const
{
    return cv::Point(1124,906);
}

std::vector<int> Resolution1680_1050::columns() const
{
    std::vector<int> cols {0, 319, 394, 473, 552, 713, 891};
    return cols;
}

QPair<int, int> Resolution1680_1050::lengthVHlines() const
{
    return QPair<int,int>(1000,600);
}

cv::Rect Resolution1680_1050::stationRect() const
{
    return cv::Rect(cv::Point(68,110),cv::Size(1058,28));
}


Resolution1768_992::Resolution1768_992()
{
    width = 1768;
    height = 992;
}

cv::Point Resolution1768_992::tl() const
{
    return cv::Point(75,221+3);
}

cv::Point Resolution1768_992::br() const
{
    return cv::Point(1182,895);
}

std::vector<int> Resolution1768_992::columns() const
{
    std::vector<int> cols {0, 335, 416, 496, 582, 750, 935};
    return cols;
}

QPair<int, int> Resolution1768_992::lengthVHlines() const
{
    return QPair<int,int>(1100,300);
}

cv::Rect Resolution1768_992::stationRect() const
{
    return cv::Rect(cv::Point(74,62),cv::Size(1110,28));
}

Resolution1920_1080::Resolution1920_1080()
{
    width = 1920;
    height = 1080;
}

cv::Point Resolution1920_1080::tl() const
{
    return cv::Point(80,242);
}

cv::Point Resolution1920_1080::br() const
{
    return cv::Point(1285,975);
}

std::vector<int> Resolution1920_1080::columns() const
{
    std::vector<int> cols {0, 364, 452, 540, 632, 815, 1017};
    return cols;
}

QPair<int, int> Resolution1920_1080::lengthVHlines() const
{
    return QPair<int,int>(1200,200);
}

cv::Rect Resolution1920_1080::stationRect() const
{
    return cv::Rect(cv::Point(78,68),cv::Size(1210,28));
}

Resolution1920_1200::Resolution1920_1200()
{
    width = 1920;
    height = 1200;
}

cv::Point Resolution1920_1200::tl() const
{
    return cv::Point(80,302);
}

cv::Point Resolution1920_1200::br() const
{
    return cv::Point(1285,1035);
}

std::vector<int> Resolution1920_1200::columns() const
{
    std::vector<int> cols {0, 364, 452, 540, 632, 815, 1017};
    return cols;
}

QPair<int, int> Resolution1920_1200::lengthVHlines() const
{
    return QPair<int,int>(1200,700);
}

cv::Rect Resolution1920_1200::stationRect() const
{
    return cv::Rect(cv::Point(78,128),cv::Size(1210,28));
}

Resolution1920_1440::Resolution1920_1440()
{
    width = 1920;
    height = 1440;
}

cv::Point Resolution1920_1440::tl() const
{
    return cv::Point(80,423);
}

cv::Point Resolution1920_1440::br() const
{
    return cv::Point(1285,1156);
}

std::vector<int> Resolution1920_1440::columns() const
{
    std::vector<int> cols {0, 364, 452, 540, 632, 815, 1017};
    return cols;
}

QPair<int, int> Resolution1920_1440::lengthVHlines() const
{
    return QPair<int,int>(1200,700);
}

cv::Rect Resolution1920_1440::stationRect() const
{
    return cv::Rect(cv::Point(78,246),cv::Size(1210,28));
}

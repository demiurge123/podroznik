#pragma once
#include <QObject>
#include <opencv2/opencv.hpp>
#include <QSharedPointer>

class Resolution
{
protected:
    int width;
    int height;
public:
    virtual ~Resolution() {}
    virtual cv::Point tl() const = 0;
    virtual cv::Point br() const = 0;
    virtual std::vector<int> columns() const = 0;
    cv::Rect rect() {return cv::Rect(tl(),br());}
    virtual QPair<int, int> lengthVHlines() const = 0;
    virtual cv::Rect stationRect() const = 0;
    virtual int maxHeightLetter() const = 0;
    virtual int minHeightLetter() const = 0;
    int getWidth() const { return width; }
};

Q_DECLARE_INTERFACE(Resolution, "local.metropolis.Podroznik")

class ResolutionNull : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    ResolutionNull() {}
    virtual ~ResolutionNull() {}
    cv::Point tl() const {return cv::Point(0,0);}
    cv::Point br() const {return cv::Point(0,0);}
    std::vector<int> columns() const {return std::vector<int>();}
    QPair<int, int> lengthVHlines() const {return QPair<int,int>(10000,10000);}
    cv::Rect stationRect() const {return cv::Rect();}
    int maxHeightLetter() const {return 0;}
    int minHeightLetter() const {return 0;}
};

class Resolution2560_1440 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution2560_1440();
    virtual ~Resolution2560_1440() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 32;}
    int minHeightLetter() const {return 20;}
};

class Resolution1680_1050 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution1680_1050();
    virtual ~Resolution1680_1050() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 20;}
    int minHeightLetter() const {return 14;}
};

class Resolution1768_992 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution1768_992();
    virtual ~Resolution1768_992() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 20;}
    int minHeightLetter() const {return 14;}
};

class Resolution1920_1080 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution1920_1080();
    virtual ~Resolution1920_1080() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 20;}
    int minHeightLetter() const {return 16;}
};

class Resolution1920_1200 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution1920_1200();
    virtual ~Resolution1920_1200() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 20;}
    int minHeightLetter() const {return 16;}
};

class Resolution1920_1440 : public QObject, public Resolution
{
    Q_OBJECT
    Q_INTERFACES(Resolution)
public:
    Resolution1920_1440();
    virtual ~Resolution1920_1440() {}
    cv::Point tl() const;
    cv::Point br() const;
    std::vector<int> columns() const;
    QPair<int, int> lengthVHlines() const;
    cv::Rect stationRect() const;
    int maxHeightLetter() const {return 20;}
    int minHeightLetter() const {return 16;}
};






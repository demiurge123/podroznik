#include "resolutionfactory.h"

QSharedPointer<Resolution> ResolutionFactoryImpl::NULLResolution(new ResolutionNull());

ResolutionFactoryImpl::ResolutionFactoryImpl()
{

}

QSharedPointer<Resolution> ResolutionFactoryImpl::make(int width, int height)
{
    if (width == 1680 && height == 1050)
        return QSharedPointer<Resolution>(new Resolution1680_1050());
    if (width == 1768 && height == 992)
        return QSharedPointer<Resolution>(new Resolution1768_992());
    if (width == 1920 && height == 1080)
        return QSharedPointer<Resolution>(new Resolution1920_1080());
    if (width == 1920 && height == 1200)
        return QSharedPointer<Resolution>(new Resolution1920_1200());
    if (width == 1920 && height == 1440)
        return QSharedPointer<Resolution>(new Resolution1920_1440());
    if (width == 2560 && height == 1440)
        return QSharedPointer<Resolution>(new Resolution2560_1440());
    return NULLResolution;
}

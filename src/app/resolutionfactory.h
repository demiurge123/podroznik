#pragma once

#include "resolution.h"

class ResolutionFactory
{
public:
    virtual QSharedPointer<Resolution> make(int width, int height) = 0;
};

Q_DECLARE_INTERFACE(ResolutionFactory, "local.metropolis.Podroznik")

class ResolutionFactoryImpl : public QObject, public ResolutionFactory
{
    Q_OBJECT
    Q_INTERFACES(ResolutionFactory)
public:
    ResolutionFactoryImpl();
    QSharedPointer<Resolution> make(int width, int height);

    static QSharedPointer<Resolution> NULLResolution;
};

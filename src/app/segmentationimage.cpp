#include "segmentationimage.h"
#include "resolutionfactory.h"
#include <QDebug>

std::vector<cv::Vec4i> detectLinesHorizontalVertical(cv::Mat src)
{
    std::vector<cv::Vec4i> lines;
    cv::Size size(3,3);
    cv::Mat invSrc =  cv::Scalar::all(255) - src;
    cv::GaussianBlur(invSrc,invSrc,size,0);
    cv::adaptiveThreshold(invSrc, invSrc,255,CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY,75,10);
    cv::bitwise_not(invSrc, invSrc);

    cv::HoughLinesP(invSrc, lines, 1, CV_PI/4, 80, 200, 3);

    return lines;
}

std::pair<int,int> getMaxLengthLines(std::vector<cv::Vec4i> lines)
{
    int maxx=0, maxy=0;
    for (int i = 0; i < (int)lines.size(); i++)
    {
        cv::Vec4i v = lines[i];
        if (maxx<std::abs(v[0]-v[2])) {
            maxx=std::abs(v[0]-v[2]);
        }
        if (maxy<std::abs(v[1]-v[3])) {
            maxy=std::abs(v[1]-v[3]);
        }
    }
    return std::make_pair(maxx,maxy);
}

bool verifySize(const cv::Rect &rect)
{
    float minHeight=9;
    float maxHeight=34;
    if(rect.height >= minHeight && rect.height < maxHeight)
        return true;
    else
        return false;
}

bool verifyContains(const cv::Mat letter, const cv::Rect &rect)
{
    int sum=0;
    for (int i=0;i<rect.width;i++) {
        int vmin=255,vmax=0;

        for (int j=0;j<rect.height;j++) {
            int value = letter.at<uchar>(j,i);
            if (value<vmin)
                vmin=value;
            if (value>vmax)
                vmax=value;
        }
        if (vmax-vmin>128 || vmin>200)
            sum++;
    }
    float procentWhitePixels = 100.0f * (float) sum / (float)(rect.width);
    return procentWhitePixels>10.0f ? true:false;
}

std::vector<cv::Rect> getRects(cv::Mat auxRoi)
{
    std::vector< std::vector< cv::Point> > contours;
    cv::Mat auxRoiContours;
    auxRoi.copyTo(auxRoiContours);

    //    cv::blur(auxRoiContours,auxRoiContours,cv::Size(2,2));


    cv::threshold(auxRoiContours, auxRoiContours, 75, 255, CV_THRESH_BINARY);
    /*
    {
        cv::Mat result;
        cv::cvtColor(auxRoiContours, result,cv::COLOR_GRAY2RGBA);
        QImage img((uchar*) result.data, result.cols, result.rows, (int)result.step, QImage::Format_RGB32);
        img.save("test1.png");
    }
*/
    //    cv::Mat kernel4 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 2));
    //    cv::erode(auxRoiContours,auxRoiContours,kernel4);

    //    {
    //        cv::Mat result;
    //        cv::cvtColor(auxRoiContours, result,cv::COLOR_GRAY2RGBA);
    //        QImage img((uchar*) result.data, result.cols, result.rows, (int)result.step, QImage::Format_RGB32);
    //        img.save("test2.png");
    //    }


    //    cv::Mat kernel2 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 2));
    //    cv::dilate(auxRoiContours,auxRoiContours,kernel2);

    //    {
    //        cv::Mat result;
    //        cv::cvtColor(auxRoiContours, result,cv::COLOR_GRAY2RGBA);
    //        QImage img((uchar*) result.data, result.cols, result.rows, (int)result.step, QImage::Format_RGB32);
    //        img.save("test3.png");
    //    }

    cv::findContours(auxRoiContours,
                     contours,            // a vector of contours
                     CV_RETR_EXTERNAL,    // retrieve the external contours
                     CV_CHAIN_APPROX_NONE); // all pixels of each contour
    std::vector<std::vector<cv::Point> >::iterator itc= contours.begin();
    std::vector<cv::Rect> rects;
    while (itc!=contours.end()) {
        cv::Rect mrr= cv::boundingRect(cv::Mat(*itc));


        // verifyContains(letter,mrr)
        if (verifySize(mrr) ) {
            if ((double)mrr.width/mrr.height >1.5) {
                cv::Mat letter(auxRoi,mrr);
                cv::threshold(letter, letter, 75, 255, CV_THRESH_BINARY);

                cv::Mat kernel4 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 2));
                cv::erode(letter,letter,kernel4);

                bool isWhite=false;
                int shift = 0;
                for(int j=1;j<mrr.width;j++) {
                    int sum=0;
                    for (int i=0;i<mrr.height;i++)
                        sum+=letter.at<uchar>(i,j);
                    if (sum==0) {
                        if (isWhite) {
                            cv::Rect crop;
                            crop.x=mrr.x+shift;
                            crop.width=j-shift;
                            crop.y=mrr.y;
                            crop.height=mrr.height;
                            rects.push_back(crop);
                            shift = j;
                            isWhite=false;
                        }
                    }
                    else
                        isWhite=true;

                }
                if (std::abs(shift-mrr.width)>2) {
                    cv::Rect crop;
                    crop.x=mrr.x+shift;
                    crop.width=mrr.width-shift;
                    crop.y=mrr.y;
                    crop.height=mrr.height;
                    rects.push_back(crop);
                }
            }
            else
                rects.push_back(mrr);
        }
        ++itc;
    }

    return rects;
}

cv::Mat preprocessChar(const cv::Mat &in, const int &sizeLetters)
{
    //Remap image
    int h=in.rows;
    int w=in.cols;
    cv::Mat transformMat=cv::Mat::eye(2,3,CV_32F);
    int m=cv::max(w,h);
    transformMat.at<float>(0,2)=m/2 - w/2;
    transformMat.at<float>(1,2)=m/2 - h/2;

    cv::Mat warpImage(m,m, in.type());
    cv::warpAffine(in, warpImage, transformMat, warpImage.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0) );

    cv::Mat out;
    cv::resize(warpImage, out, cv::Size(sizeLetters, sizeLetters) );

    return out;
}

bool sortRect(const cv::Rect& rec1,const cv::Rect& rec2) {
    return rec1.x< rec2.x?true:false;
}

int getSizeLetters(const int& width) {
    switch (width) {
    case 1680:
    case 1768:
        return 15;
        break;
    case 1920:
        return 20;
        break;
    case 2560:
        return 25;
        break;
    }
    return 20;
}

QSharedPointer<SegmentationImage> toSegmentation(QSharedPointer<PreparationImage> preparation)
{

    ResolutionFactoryImpl factory;
    QSharedPointer<Resolution> resolution = factory.make(preparation->width(),preparation->height());
    const std::vector<int> columns = resolution->columns();
    cv::Point tl = resolution->tl(), br = resolution->br();
    int sizeLetters = getSizeLetters(resolution->getWidth());

    cv::Mat src = preparation->getImage();
    std::vector<cv::Vec4i> lines= detectLinesHorizontalVertical(src);
    std::pair<int,int> maxLengthLines = getMaxLengthLines(lines);
    //    qDebug()<<maxLengthLines.first<<maxLengthLines.second;
    QMap<int,cv::Mat> images;
    mapRects mrects;
    const QPair<int,int> lengthVHlines = resolution->lengthVHlines();

    if ( maxLengthLines.first>lengthVHlines.first && maxLengthLines.second>lengthVHlines.second ){

        QScopedArrayPointer<int> horizontalAvg(new int[std::abs(br.y-tl.y)+1]);

        for (int i=tl.y;i<=br.y;i++) {
            int index=i-tl.y;
            horizontalAvg[index]=0;
            for (int j=tl.x;j<=br.x;j++)
                horizontalAvg[index]+=src.at<uchar>(i,j);
            horizontalAvg[index]/=(br.x-tl.x+1);
        }

        bool last=false,first = true;
        int lastposition=-1;


        std::stack<std::vector<cv::Mat> > stack;
        std::stack<std::vector<QRect> > stackRects;

        for (int i=br.y;i>=tl.y;i--) {
            if (!last && horizontalAvg[i-tl.y]>=2) {
                if (first || std::abs(lastposition-i)>=resolution->maxHeightLetter()) {
                    last=true;
                    lastposition=i;
                    first=false;
                }
            } else
                if (last && (horizontalAvg[i-tl.y]<=1 || i-tl.y<=2)){
                    if (std::abs(lastposition-i)>=resolution->minHeightLetter()) {
                        last=false;

                        std::vector<cv::Mat> vecLineImg;
                        std::vector<QRect> vecLineRect;
                        for (int numberColumn=1;numberColumn<=(int)columns.size();numberColumn++) {
                            cv::Rect mr;
                            mr.x=tl.x+columns.at(numberColumn-1);
                            mr.y=i;
                            if (numberColumn<(int)columns.size())
                                mr.width=columns.at(numberColumn)-columns.at(numberColumn-1);
                            else
                                mr.width=br.x-tl.x-columns.at(columns.size()-1);
                            mr.height=lastposition-i+2;
                            cv::Mat auxRoi(src, mr);
                            std::vector<cv::Rect> rects = getRects(auxRoi);

                            if (rects.size()>0) {

                                cv::Mat result=cv::Mat::zeros(sizeLetters, (int)rects.size()*sizeLetters, CV_8UC1);

                                qSort(rects.begin(),rects.end(),sortRect);
                                QRect rect;
                                for(int k = 0; k<(int)rects.size(); ++k) {
                                    cv::Rect r=rects.at(k);
                                    QRect currentRect = QRect(tl.x+r.x,tl.y+r.y,r.width,r.height);
                                    if (!rect.isEmpty())
                                        rect = rect.united(currentRect);
                                    else
                                        rect = currentRect;
                                    cv::Mat imgROI(auxRoi,r);
                                    imgROI=preprocessChar(imgROI,sizeLetters);
                                    cv::Mat place(result, cv::Rect(k*sizeLetters, 0, sizeLetters, sizeLetters));
                                    imgROI.copyTo(place);
                                }
                                vecLineImg.push_back(result);
                                vecLineRect.push_back(rect);
                                //                                images.insert((line-1)*numberColumns+column+1,result);
                            } else {
                                vecLineImg.push_back(cv::Mat());
                                vecLineRect.push_back(QRect());
                            }
                        }

                        stack.push(vecLineImg);
                        stackRects.push(vecLineRect);

                        lastposition=i;
                        if (i-tl.y<2)
                            break;
                    }
                }
        }

        int line=0;
        while(!stack.empty()) {
            std::vector<cv::Mat> vector = stack.top();
            stack.pop();

            std::vector<QRect> vectorRect = stackRects.top();
            stackRects.pop();

            for (int i=0;i<(int)vector.size();i++) {
                cv::Mat result = vector.at(i);
                QRect resultRect = vectorRect.at(i);
                if (!result.empty()) {
                    images.insert(line*columns.size()+i+1,result);
                    mrects.insert(line*columns.size()+i+1,resultRect);
                }
            }
            vector.clear();
            vectorRect.clear();
            line++;
        }

        cv::Rect mr = resolution->stationRect();

        cv::Mat auxRoi(src, mr);
        std::vector<cv::Rect> rects = getRects(auxRoi);
        if (rects.size()>0) {
            cv::Mat result=cv::Mat::zeros(sizeLetters, (int)rects.size()*sizeLetters, CV_8UC1);
            QRect resultRect;
            qSort(rects.begin(),rects.end(),sortRect);
            for(int k = 0; k<(int)rects.size(); ++k) {
                cv::Rect r=rects.at(k);
                QRect currentRect = QRect(mr.x+r.x,mr.y+r.y,r.width,r.height);
                if (!resultRect.isEmpty())
                    resultRect = resultRect.united(currentRect);
                else
                    resultRect = currentRect;
                cv::Mat imgROI(auxRoi,r);
                imgROI=preprocessChar(imgROI,sizeLetters);
                cv::Mat place(result, cv::Rect(k*sizeLetters, 0, sizeLetters, sizeLetters));
                imgROI.copyTo(place);
            }
            images.insert(0,result);
            mrects.insert(0,resultRect);
        }

    }


    return QSharedPointer<SegmentationImage>(new SegmentationImage(images,mrects,preparation->getPath()));
}


void SegmentationImage::save(int key)
{
    QMapIterator<int, cv::Mat> i(images);
    while(i.hasNext()) {
        i.next();
        cv::Mat src=i.value();
        cv::cvtColor(src, src,cv::COLOR_GRAY2RGBA);
        QImage img((uchar*) src.data, src.cols, src.rows, (int)src.step, QImage::Format_RGB32);
        QFile file(path);
        QFileInfo fileInfo(file);
        int line;
        int column;
        if (i.key()==0) {
            line=column=0;
        } else {
            line = ((i.key()-1)/7)+1;
            column = ((i.key()-1)%7)+1;
        }
        //        img.save(QString("%1/_%2_%3_%4.png").arg(fileInfo.absolutePath()).arg(fileInfo.baseName()).arg(line).arg(column));
        if (key==-1 || i.key()==key)
            img.save(QString("/Volumes/dane/test1920/1920_%1_%2_%3.png").arg(fileInfo.baseName()).arg(line).arg(column));
    }

}

int SegmentationImage::getLength(int number)
{
    cv::Mat image = images.value(number);
    return image.cols/image.rows;
}

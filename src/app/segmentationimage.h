#pragma once
#include <opencv2/opencv.hpp>
#include <QFileInfo>
#include <QMap>
#include <stack>
#include "preparationimage.h"

typedef QMap< int, QRect> mapRects;
Q_DECLARE_METATYPE(mapRects)


class SegmentationImage
{
private:
    QMap<int,cv::Mat> images;
    mapRects m_rects;
    QString path;

public:
    SegmentationImage(QMap<int,cv::Mat> images=QMap<int,cv::Mat>(),
                      mapRects rects=mapRects(),
                      const QString& path=QString())
        :path(path) { this->images.swap(images); this->m_rects.swap(rects); }
    void save(int key=-1);
    QString getPath() const { return path; }
    QMap<int, cv::Mat> getImages() const { return images; }
    mapRects getRects() const { return m_rects; }
    int getLength(int number);
};

std::vector<cv::Vec4i> detectLinesHorizontalVertical(cv::Mat src);
std::pair<int,int> getMaxLengthLines(std::vector<cv::Vec4i> lines);
bool verifySize(const cv::Rect &rect);
bool verifyContains(const cv::Mat letter, const cv::Rect &rect);
std::vector<cv::Rect> getRects(cv::Mat auxRoi);
cv::Mat preprocessChar(const cv::Mat &in,const int& sizeLetters);
QSharedPointer<SegmentationImage> toSegmentation(QSharedPointer<PreparationImage> preparation);

#include <QDir>
#include <QStandardPaths>
#include <QStringList>
#include <QCoreApplication>
#include <QDebug>
#include "appfolder.h"

#include "trainer.h"


Trainer::Trainer(const QString& pathToYML)
    :pathToYML(pathToYML)
{
    if (!pathToYML.isEmpty()) {
        QFile trainFile(pathToYML);
        if (trainFile.exists()) {
            loadTrain();
        }
    }
}

Trainer::Trainer(const int &sizeLetters)
{
    QString appFolder = getDirApplication();
    pathToYML = appFolder+QString("data/mlp%1.yml").arg(sizeLetters);
    QFile trainFile(pathToYML);
    if (trainFile.exists()) {
        loadTrain();
    }
}

void Trainer::loadTrain()
{
    ann.load(pathToYML.toStdString().c_str());
    chars = QVector<QString>()<<"0"<< "1"<< "2"<< "3"<< "4"<< "5"<< "6"<< "7"<< "8"<< "9"<<
                                "A"<< "B"<< "C"<< "D"<< "E"<< "F"<< "G"<< "H"<< "I"<< "J"<<
                                "K"<< "L"<< "M"<< "N"<< "O"<< "P"<< "Q"<< "R"<< "S"<< "T"<<
                                "U"<< "V"<< "W"<< "X"<< "Y"<< "Z";
}

cv::Mat Trainer::features(cv::Mat in, const int &sizeLetters)
{
    //Histogram features
    cv::Mat vhist=ProjectedHistogram(in,VERTICAL);
    cv::Mat hhist=ProjectedHistogram(in,HORIZONTAL);

    //Low data feature
    cv::Mat lowData;
    cv::resize(in, lowData, cv::Size(sizeLetters, sizeLetters) );

    //Last 10 is the number of moments components
    int numCols=vhist.cols+hhist.cols+lowData.cols*lowData.cols;

    cv::Mat out=cv::Mat::zeros(1,numCols,CV_32F);
    //Asign values to feature
    int j=0;
    for(int i=0; i<vhist.cols; i++)
    {
        out.at<float>(j)=vhist.at<float>(i);
        j++;
    }
    for(int i=0; i<hhist.cols; i++)
    {
        out.at<float>(j)=hhist.at<float>(i);
        j++;
    }
    for(int x=0; x<lowData.cols; x++)
    {
        for(int y=0; y<lowData.rows; y++){
            out.at<float>(j)=(float)lowData.at<unsigned char>(x,y);
            j++;
        }
    }

    return out;
}

char Trainer::classify(cv::Mat f)
{
    cv::Mat output(1, 36, CV_32FC1);
    ann.predict(f, output);
    cv::Point maxLoc;
    double maxVal;
    minMaxLoc(output, 0, &maxVal, 0, &maxLoc);
    if (maxLoc.x<chars.size())
        return chars.at(maxLoc.x).toStdString().c_str()[0];
    return (char)0;
}

QString Trainer::recognitionImage(cv::Mat image)
{
    QString recognitionText=QString();
    int countLetterRecognized = image.cols/image.rows;
    for (int i=0;i<countLetterRecognized;i++) {
        cv::Mat img = cv::Mat(image,cv::Rect(i*image.rows,0,image.rows,image.rows));

        cv::Mat f=features(img,image.rows);
        char znak = classify(f);
        if (znak!=0)
            recognitionText.append(znak);
    }
    return recognitionText;
}

cv::Mat Trainer::ProjectedHistogram(cv::Mat img, int t)
{
    int sz=(t)?img.rows:img.cols;
    cv::Mat mhist=cv::Mat::zeros(1,sz,CV_32F);

    for(int j=0; j<sz; j++){
        cv::Mat data=(t)?img.row(j):img.col(j);
        mhist.at<float>(j)=countNonZero(data);
    }

    //Normalize histogram
    double min, max;
    minMaxLoc(mhist, &min, &max);

    if(max>0)
        mhist.convertTo(mhist,-1 , 1.0f/max, 0);

    return mhist;
}

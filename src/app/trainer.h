#pragma once
#include <QObject>
#include <QVector>
#include <QSharedPointer>
#include <opencv2/opencv.hpp>

class Trainer
{
public:
    Trainer(const QString &pathToYML=QString());
    Trainer(const int &sizeLetters);
    void loadTrain();
    cv::Mat ProjectedHistogram(cv::Mat img, int t);
    cv::Mat features(cv::Mat in, const int &sizeLetters);
    char classify(cv::Mat f);
    QString recognitionImage(cv::Mat image);
    double getQuality();
private:
    QString pathToYML;
    QVector<QString> chars;
    CvANN_MLP  ann;
    CvKNearest knnClassifier;
    enum DIRECTION {VERTICAL, HORIZONTAL};
    double allQuality;
    int counterQuality;
};


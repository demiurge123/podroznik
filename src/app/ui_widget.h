/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QTabWidget *tabWidget;
    QWidget *Commodities;
    QGridLayout *gridLayout_2;
    QTableView *tableCommodities;
    QWidget *Stations;
    QGridLayout *gridLayout_4;
    QTreeView *treeView;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QGridLayout *gridLayout_3;
    QGraphicsView *graphicsView;
    QHBoxLayout *horizontalLayout;
    QToolButton *folderWithScreenshots;
    QSpacerItem *horizontalSpacer;
    QLineEdit *lineEdit;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(1383, 774);
        gridLayout = new QGridLayout(Widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        splitter = new QSplitter(Widget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setFrameShape(QFrame::NoFrame);
        splitter->setFrameShadow(QFrame::Sunken);
        splitter->setLineWidth(0);
        splitter->setOrientation(Qt::Horizontal);
        splitter->setHandleWidth(0);
        tabWidget = new QTabWidget(splitter);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setMaximumSize(QSize(500, 16777215));
        Commodities = new QWidget();
        Commodities->setObjectName(QStringLiteral("Commodities"));
        gridLayout_2 = new QGridLayout(Commodities);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        tableCommodities = new QTableView(Commodities);
        tableCommodities->setObjectName(QStringLiteral("tableCommodities"));
        tableCommodities->setEnabled(true);
        tableCommodities->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableCommodities->setSelectionMode(QAbstractItemView::SingleSelection);
        tableCommodities->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridLayout_2->addWidget(tableCommodities, 0, 0, 1, 1);

        tabWidget->addTab(Commodities, QString());
        Stations = new QWidget();
        Stations->setObjectName(QStringLiteral("Stations"));
        Stations->setEnabled(true);
        gridLayout_4 = new QGridLayout(Stations);
        gridLayout_4->setSpacing(0);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        treeView = new QTreeView(Stations);
        treeView->setObjectName(QStringLiteral("treeView"));

        gridLayout_4->addWidget(treeView, 0, 0, 1, 1);

        tabWidget->addTab(Stations, QString());
        splitter->addWidget(tabWidget);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(layoutWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setAutoFillBackground(false);
        frame->setStyleSheet(QStringLiteral(""));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Plain);
        frame->setLineWidth(0);
        gridLayout_3 = new QGridLayout(frame);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(2, 4, 2, 4);
        graphicsView = new QGraphicsView(frame);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));

        gridLayout_3->addWidget(graphicsView, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        folderWithScreenshots = new QToolButton(frame);
        folderWithScreenshots->setObjectName(QStringLiteral("folderWithScreenshots"));
        folderWithScreenshots->setToolButtonStyle(Qt::ToolButtonIconOnly);

        horizontalLayout->addWidget(folderWithScreenshots);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        lineEdit = new QLineEdit(frame);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setMinimumSize(QSize(0, 0));
        lineEdit->setFrame(true);

        horizontalLayout->addWidget(lineEdit);


        gridLayout_3->addLayout(horizontalLayout, 0, 0, 1, 1);


        verticalLayout->addWidget(frame);

        splitter->addWidget(layoutWidget);

        gridLayout->addWidget(splitter, 0, 0, 1, 1);


        retranslateUi(Widget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "StellarTraveller", 0));
        tabWidget->setTabText(tabWidget->indexOf(Commodities), QApplication::translate("Widget", "Commodities", 0));
        tabWidget->setTabText(tabWidget->indexOf(Stations), QApplication::translate("Widget", "Stations", 0));
        folderWithScreenshots->setText(QApplication::translate("Widget", "...", 0));
        lineEdit->setPlaceholderText(QApplication::translate("Widget", "Filter", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H

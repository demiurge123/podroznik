#include <QSqlError>
#include <QFileDialog>
#include <QFuture>
#include <QtConcurrent>
#include <QDebug>
#include <QMessageBox>
#include "widget.h"
#include "ui_widget.h"
#include "ui_systems.h"
#include "appfolder.h"

#define CONCURRENT

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    monitorOn(false)
{
    ui->setupUi(this);
    m_database.reset(new Database("StellarTraveller.db"));

    setWindowTitle(QString("Stellar Traveller - %1 by demiurge").arg(qApp->applicationVersion()));
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    commoditiesModel = new QSqlTableModel(this,QSqlDatabase::database());
    commoditiesModel->setTable("vPrices");
    //    commoditiesModel->select();

    ui->tableCommodities->setModel(commoditiesModel);
    ui->tableCommodities->hideColumn(0);
    ui->tableCommodities->hideColumn(1);
    ui->tableCommodities->hideColumn(5);
    ui->tableCommodities->resizeColumnsToContents();
    ui->tableCommodities->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    world = new World(this);
    completer = new QCompleter(this);
    completer->setModel(world);
    completer->setFilterMode(Qt::MatchContains);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setCompletionColumn(0);
    connect(completer,SIGNAL(activated(QModelIndex)),this,SLOT(activated(QModelIndex)));
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(firstLetterEdit(QString)));
    ui->lineEdit->setCompleter(completer);


    monitor= new QFileSystemWatcher(this);
    connect(ui->folderWithScreenshots,SIGNAL(clicked()),this,SLOT(folderWithScreenshots()));
    connect(monitor,SIGNAL(directoryChanged(QString)),this,SLOT(newFileInFolder(QString)));

    QString appDir = getDirApplication();
    monitor->addPath(appDir);

    CorrectionText::stations=prepareStationName(m_database.data());
    CorrectionText::commodities=prepareCommodityName(m_database.data());
    CorrectionText::categories=prepareCategoryCommodityName(m_database.data());


}

Widget::~Widget()
{
    QStringList folders = monitor->directories();
    if (folders.size()>0)
        monitor->removePaths(folders);
    delete monitor;
    delete commoditiesModel;
    delete scene;
    delete ui;
}

void Widget::activated(const QModelIndex &index)
{
    qDebug()<<index.data().toString()<<index.sibling(index.row(),1).data().toString();
    commoditiesModel->setFilter(QString("station = \"%1\" AND system = \"%2\"").arg(index.data().toString()).arg(index.sibling(index.row(),1).data().toString()));
    qDebug()<<commoditiesModel->select()<<commoditiesModel->filter();
}

void Widget::folderWithScreenshots()
{
    QString folderName = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                           ".",
                                                           QFileDialog::ShowDirsOnly
                                                           | QFileDialog::DontResolveSymlinks);
    if (!folderName.isEmpty() && !monitor->directories().contains(folderName)) {
        monitor->addPath(folderName);
    }
}

void Widget::addPathOverTime(const QString &path, const int &msec)
{
    if (!m_paths.contains(path)) {
        m_paths.append(path);
        timer.start(msec,this);
    }
}

void Widget::removePathAndStopTimer(const QString &path)
{
    if (m_paths.contains(path))
        m_paths.removeAll(path);
    if (m_paths.size()==0 && timer.isActive())
        timer.stop();
}

void Widget::clearResults()
{
    m_corrections.clear();
    m_recognitions.clear();
    m_segmentations.clear();
    m_preparations.clear();
    m_originals.clear();
}

void Widget::upgradeMarketData(const QStringList &market)
{
    if (!m_markets.contains(market.first()))
        m_markets[market.first()]=makeMarket();
    QStringList fullMarket = m_markets[market.first()];
    int beginFullMarket=0;
    for (int j=1;j<market.size();j++) {
        QString itemMarket = market.at(j).split(":").at(0);
        for (int i=beginFullMarket;i<fullMarket.size();i++) {
            QString itemFullMarket=fullMarket.at(i).split(":").at(0);
            if (itemMarket.compare(itemFullMarket)==0) {
                fullMarket[i]=market.at(j);
                beginFullMarket=i+1;
                break;
            }
        }
    }
    m_markets[market.first()]=fullMarket;
}



void Widget::newFileInFolder(const QString &path)
{
    if (!monitorOn) {
        monitorOn=true;
        QDir folderSource(path);
        QStringList filters;
        filters << "*.jpg"<<"*.jpeg"<<"*.png"<<"*.tiff"<<"*.bmp";

        folderSource.setNameFilters(filters);
        m_originals.clear();
        QThread::msleep(1000);
        QStringList filenames=folderSource.entryList(QDir::Files);
        
        foreach (QString filename,filenames) {
            QString path = folderSource.absolutePath()+"/"+filename;
            if (QFile(path).exists()) {
                QImage img(path);
                m_originals.append(QSharedPointer<OriginalImage>(new OriginalImage(img.copy(),path)));
            }
            //            if (m_originals.size()==1)
            //                break;
        }
        m_count = m_originals.size();
        if (m_count>0) {
#ifdef CONCURRENT
            m_preparations = QtConcurrent::blockingMapped(m_originals, toPreparation);
            m_segmentations = QtConcurrent::blockingMapped(m_preparations, toSegmentation);
            m_recognitions = QtConcurrent::blockingMapped(m_segmentations, toRecognition);
            m_corrections = QtConcurrent::blockingMapped(m_recognitions, toCorrection);
#else
            foreach(QSharedPointer<OriginalImage> original,m_originals) {
                QSharedPointer<PreparationImage> preparation = toPreparation(original);
                m_preparations.append(preparation);
            }

            foreach(QSharedPointer<PreparationImage> preparation,m_preparations) {
                QSharedPointer<SegmentationImage> segmentation = toSegmentation(preparation);
                m_segmentations.append(segmentation);
            }

            foreach(QSharedPointer<SegmentationImage> segmentation,m_segmentations) {
                QSharedPointer<RecognitionText> recognition = toRecognition(segmentation);
                m_recognitions.append(recognition);
            }

            foreach(QSharedPointer<RecognitionText> recognition,m_recognitions) {
                QSharedPointer<CorrectionText> correction = toCorrection(recognition);
                m_corrections.append(correction);
            }
#endif
            foreach(QSharedPointer<CorrectionText> correction,m_corrections) {
                QStringList market = correction->getList();
                upgradeMarketData(market);
                saveMarketData(market.first());
            }

            foreach(QSharedPointer<OriginalImage> original,m_originals) {
                QString path = original->getPath();
                QFile file(path);
                file.remove();
            }

            removePathAndStopTimer(path);

            clearResults();
        }
        monitorOn=false;
    } else
        addPathOverTime(path,2000);
}

void Widget::firstLetterEdit(const QString &text)
{
    if (text.length()==1) {
        if (text.at(0)==QChar('@')) {
            world->setScope(System);
            ui->tabWidget->setTabEnabled(0,false);
            ui->tabWidget->setTabEnabled(1,true);
            ui->tabWidget->setCurrentIndex(1);
        }
        else {
            world->setScope(Station);
            ui->tabWidget->setTabEnabled(0,true);
            ui->tabWidget->setTabEnabled(1,false);
            ui->tabWidget->setCurrentIndex(0);
        }
    }
}

void Widget::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer.timerId()) {
        foreach (QString path,m_paths)
            newFileInFolder(path);
    }
}

void Widget::printHeader(const QString& nameMarket, QTextStream& out)
{
    QPair<QString,QString> nameStationAndSystem = getNameStationAndSystem(nameMarket);
    out<<QString("# TradeDangerous prices for ['%1', '%2']").arg(nameStationAndSystem.first).arg(nameStationAndSystem.second)<<endl;
    out<<endl;
    out<<"# REMOVE ITEMS THAT DON'T APPEAR IN THE UI"<<endl;
    out<<"# ORDER IS REMEMBERED: Move items around within categories to match the game UI"<<endl;
    out<<endl;
    out<<"# File syntax:"<<endl;
    out<<"# <item name> <sell> <buy> [<demand> <stock> [<timestamp>]]"<<endl;
    out<<"#   Use '?' for demand/stock when you don't know/care,"<<endl;
    out<<"#   Use '-' for demand/stock to indicate unavailable,"<<endl;
    out<<"#   Otherwise use a number followed by L, M or H, e.g."<<endl;
    out<<"#     1L, 23M or 30000H"<<endl;
    out<<"# If you omit the timestamp, the current time will be used when the file is loaded."<<endl;
    out<<endl;
    out<<"#     Item Name                SellCr   BuyCr     Demand     Stock"<<endl;
    out<<endl;
    out<<endl;
    out<<nameMarket<<endl;
}

QPair<QString, QString> Widget::fixNameStation(QString nameStation, QString nameSystem)
{
    QPair<QString,QString> tempStationAndSystem;
    if (nameStation.compare("unknown")==0 && nameSystem.compare("unknown")==0)  {
        tempStationAndSystem = createNewStation();
        m_database->createStation(tempStationAndSystem.first, tempStationAndSystem.second);
        CorrectionText::stations.reset(nullptr);
        CorrectionText::stations=prepareStationName(m_database.data());
    } else {
        tempStationAndSystem.first=nameStation;
        tempStationAndSystem.second=nameSystem;
    }

    return tempStationAndSystem;
}

QPair<QString,QString> Widget::getNameStationAndSystem(const QString& nameMarket)
{
    QString nameStation = "unknown",
            nameSystem = "unknown";
    QRegExp regexp("@ (.*)/(.*)");
    if (nameMarket.indexOf(regexp)>-1) {
        nameStation = regexp.cap(2);
        nameSystem = regexp.cap(1);
    }

    return fixNameStation(nameStation, nameSystem);
}

QString Widget::fixCategoryName(QString commodity)
{
    return QString("   ")+commodity;
}

QString Widget::fixCommodityName(QString commodity)
{
    if (commodity.compare("Consumer Technology")==0)
        commodity = "Consumer Tech";
    return QString("      ")+commodity.leftJustified(23);
}

QString Widget::fixSellPrice(QString sell)
{
    return sell.rightJustified(8);
}

QString Widget::fixBuyPrice(QString buy)
{
    if (buy.isEmpty())
        buy = "0";
    return buy.rightJustified(8);
}

QString Widget::fixDemandLevel(QString demand)
{
    if (demand.size()<=1)
        demand = "?";
    return demand.rightJustified(11);
}

QString Widget::fixSupplyLevel(QString supply)
{
    if (supply.isEmpty())
        supply = "-";
    return supply.rightJustified(10);
}

bool Widget::isCategoryName(QString text)
{
    return text.startsWith("+ ");
}

void Widget::printCommodity(const QString& line, QTextStream& out)
{
    QStringList columns=line.split(":");
    if ( isCategoryName( columns.at(0) ) ) {
        out<<fixCategoryName(columns.at(0))<<endl;
    } else
        if (!columns.at(1).isEmpty())
        {
            out<< fixCommodityName(columns.at(0))
               << fixSellPrice(columns.at(1))
               << fixBuyPrice(columns.at(2))
               << fixDemandLevel(columns.at(4))
               << fixSupplyLevel(columns.at(5))
               << endl;
        }
}

QPair<QString, QString> Widget::createNewStation()
{
    QPair<QString,QString> market;
    QScopedPointer<QDialog> dialog(new QDialog);
    QScopedPointer<Ui::Systems> systems(new Ui::Systems);
    systems->setupUi(dialog.data());
    QStringList systemList=m_database->getAllSystemName();
    systems->comboBox->addItems(systemList);
    dialog->setWindowTitle("Create station");
    dialog->setModal(true);
    if (dialog->exec() == QDialog::Accepted) {
        QString nameStation = systems->lineEdit->text();
        QString nameSystem = systems->comboBox->currentText();
        if (!nameStation.isEmpty() && !nameSystem.isEmpty())
            market = QPair<QString,QString>(nameStation,nameSystem);
    }

    return market;
}

void Widget::saveMarketData(const QString& market)
{
    QString path = getDirApplication();
    QDir dir(path);

    QStringList data = m_markets.value(market,QStringList());
    if (data.length()>0) {
        QPair<QString,QString> nameStationAndSystem = getNameStationAndSystem(market);

        QString nameMarket = QString("@ %1/%2").arg(nameStationAndSystem.second,nameStationAndSystem.first);
        QString filename = dir.absolutePath()+QString("/%1.price").arg(nameStationAndSystem.first);
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly|QIODevice::Text)) {
            QTextStream out(&file);

            printHeader(nameMarket, out);
            for (int i=0;i<data.size();i++) {
                QString line = data.at(i);
                printCommodity(line, out);
            }
            file.close();
        }
    }
}

void Widget::saveAllRecognitionText(QList<RecognitionText> recognitions)
{
    QFile output("recognitions.txt");
    if (output.open(QIODevice::WriteOnly|QIODevice::Text)) {
        QTextStream out(&output);
        foreach(RecognitionText recognition,recognitions) {
            QString path = recognition.getPath();
            QFile file(path);
            QFileInfo fileInfo(file);

            QMap<int, QString> texts = recognition.getTexts();
            QMapIterator<int,QString> i(texts);
            while(i.hasNext()) {
                i.next();
                int line;
                int column;
                if (i.key()==0) {
                    line=column=0;
                } else {
                    line = ((i.key()-1)/7)+1;
                    column = ((i.key()-1)%7)+1;
                }

                out<< line << column << i.value()<<endl;
            }
        }
        output.close();
    }
}

QStringList Widget::makeMarket()
{
    QStringList market;
    market<<"+ Chemicals::::::"<<"Explosives::::::"<<"Hydrogen Fuel::::::"<<"Mineral Oil::::::"<<"Pesticides::::::"
         <<"+ Consumer Items::::::"<<"Clothing::::::"<<"Consumer Technology::::::"<<"Dom. Appliances::::::"
        <<"+ Foods::::::"<<"Algae::::::"<<"Animal Meat::::::"<<"Coffee::::::"<<"Fish::::::"<<"Food Cartridges::::::"<<"Fruit and Vegetables::::::"<<"Grain::::::"<<"Synthetic Meat::::::"<<"Tea::::::"
       <<"+ Industrial Materials::::::"<<"Alloys::::::"<<"Plastics::::::"<<"Polymers::::::"<<"Semiconductors::::::"<<"Superconductors::::::"
      <<"+ Legal Drugs::::::"<<"Beer::::::"<<"Liquor::::::"<<"Narcotics::::::"<<"Tobacco::::::"<<"Wine::::::"
     <<"+ Machinery::::::"<<"Atmospheric Processors::::::"<<"Crop Harvesters::::::"<<"Marine Equipment::::::"<<"Microbial Furnaces::::::"<<"Mineral Extractors::::::"<<"Power Generators::::::"<<"Water Purifiers::::::"
    <<"+ Medicines::::::"<<"Agri-Medicines::::::"<<"Basic Medicines::::::"<<"Combat Stabilisers::::::"<<"Performance Enhancers::::::"<<"Progenitor Cells::::::"
    <<"+ Metals::::::"<<"Aluminium::::::"<<"Beryllium::::::"<<"Cobalt::::::"<<"Copper::::::"<<"Gallium::::::"<<"Gold::::::"<<"Indium::::::"<<"Lithium::::::"<<"Palladium::::::"<<"Platinum::::::"<<"Silver::::::"<<"Tantalum::::::"<<"Titanium::::::"<<"Uranium::::::"
    <<"+ Minerals::::::"<<"Bauxite::::::"<<"Bertrandite::::::"<<"Coltan::::::"<<"Gallite::::::"<<"Indite::::::"<<"Lepidolite::::::"<<"Rutile::::::"<<"Uraninite::::::"
    <<"+ Technology::::::"<<"Advanced Catalysers::::::"<<"Animal Monitors::::::"<<"Aquaponic Systems::::::"<<"Auto-Fabricators::::::"<<"Bioreducing Lichen::::::"<<"Computer Components::::::"<<"H.E. Suits::::::"<<"Land Enrichment Systems::::::"<<"Resonating Separators::::::"<<"Robotics::::::"
    <<"+ Textiles::::::"<<"Cotton::::::"<<"Leather::::::"<<"Natural Fabrics::::::"<<"Synthetic Fabrics::::::"
    <<"+ Waste::::::"<<"Biowaste::::::"<<"Chemical Waste::::::"<<"Scrap::::::"
    <<"+ Weapons::::::"<<"Battle Weapons::::::"<<"Non-Lethal Weapons::::::"<<"Personal Weapons::::::"<<"Reactive Armour::::::";
    return market;
}



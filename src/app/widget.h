#pragma once
#include <QWidget>
#include <QGraphicsScene>
#include <QSqlTableModel>
#include <QCompleter>
#include <QFileSystemWatcher>
#include <QFutureWatcher>
#include <QBasicTimer>
#include "correctiontext.h"
#include "database.h"
#include "preparationimage.h"
#include "recognitiontext.h"
#include "segmentationimage.h"
#include "world.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);

    ~Widget();

    void saveAllRecognitionText(QList<RecognitionText> recognitions);

private:
    void printHeader(const QString& nameMarket, QTextStream &out);
    QPair<QString,QString> getNameStationAndSystem(const QString& nameMarket);

    void printCommodity(const QString &line, QTextStream &out);
    void addPathOverTime(const QString &path, const int& msec);
    void removePathAndStopTimer(const QString &path);
    void clearResults();
    void upgradeMarketData(const QStringList &market);
    QPair<QString, QString> createNewStation();
    QPair<QString, QString> fixNameStation(QString nameStation, QString nameSystem);

    QString fixCategoryName(QString commodity);
    QString fixCommodityName(QString commodity);
    QString fixSellPrice(QString sell);
    QString fixBuyPrice(QString buy);
    QString fixDemandLevel(QString demand);
    QString fixSupplyLevel(QString supply);

    bool isCategoryName(QString text);

    Ui::Widget *ui;
    QGraphicsScene *scene;
    QSqlTableModel *commoditiesModel;
    QCompleter *completer;
    World *world;
    QFileSystemWatcher *monitor;
    QSharedPointer<Database> m_database;
    bool monitorOn;
    QList<QSharedPointer<OriginalImage> > m_originals;
    QList<QSharedPointer<PreparationImage> > m_preparations;
    QList<QSharedPointer<SegmentationImage> > m_segmentations;
    QList<QSharedPointer<RecognitionText> > m_recognitions;
    QList<QSharedPointer<CorrectionText> > m_corrections;

    QMap<QString,QStringList> m_markets;

    QStringList makeMarket();
    void saveMarketData(const QString& market);

    int m_count;
    QBasicTimer timer;
    QStringList m_paths;

private slots:
    void activated(const QModelIndex & index);
    void folderWithScreenshots();
    void newFileInFolder(const QString& path);
    void firstLetterEdit(const QString& text);
protected:
    void timerEvent(QTimerEvent *event);
};



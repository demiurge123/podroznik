#include <QDebug>
#include "database.h"
#include "world.h"

World::World(QObject * parent)
    :QSqlQueryModel(parent),
      scope(Station)
{

}

void World::setScope(const Scope &scope)
{
    this->scope=scope;
    switch (this->scope) {
    case System:
        setQuery("SELECT name FROM System;",QSqlDatabase::database());
        break;
    case Station:
        setQuery("SELECT name, system FROM Station;",QSqlDatabase::database());
        break;
    default:
        break;
    }
}

World::~World()
{

}

QVariant World::data(const QModelIndex &index, int role) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if (role==Qt::EditRole) {
        switch (scope) {
        case System: {
            QString system = index.data().toString();
            return QVariant(QString("@%1").arg(system));
            break;
        }
        case Station: {
            QString name = index.data().toString();
            QString system = index.sibling(index.row(),1).data().toString();
            return QVariant(QString("@%1/%2").arg(system).arg(name));
            break;
        }
        default:
            return QVariant();
            break;
        }
    }
    return value;
}

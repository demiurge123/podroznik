#pragma once
#include <QSqlQueryModel>

enum Scope { System, Station };

class World : public QSqlQueryModel
{
public:
    World(QObject * parent = 0);
    void setScope(const Scope& scope);
    virtual ~World();
private:
    Scope scope;
    // QAbstractItemModel interface   
public:
    QVariant data(const QModelIndex &index, int role) const;
};




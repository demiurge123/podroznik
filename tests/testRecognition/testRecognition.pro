QT       += testlib gui
CONFIG += c++11

TARGET = tst_recognitiontest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DEPENDPATH += . \
    ../../src/app

INCLUDEPATH += . \
    ../../src/app

SOURCES += tst_recognitiontest.cpp \
    ../../src/app/preparationimage.cpp \
    ../../src/app/resolution.cpp \
    ../../src/app/resolutionfactory.cpp \
    ../../src/app/segmentationimage.cpp \
    ../../src/app/recognitiontext.cpp \
    ../../src/app/trainer.cpp \
    ../../src/app/appfolder.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../../src/app/preparationimage.h \
    ../../src/app/originalimage.h \
    ../../src/app/resolution.h \
    ../../src/app/resolutionfactory.h \
    ../../src/app/segmentationimage.h \
    ../../src/app/recognitiontext.h \
    ../../src/app/trainer.h \
    ../../src/app/appfolder.h

include(../../library.pri)

#include <QtTest>
#include <QString>
#include <QJsonDocument>
#include <QPainter>
#include <QRgb>
#include "originalimage.h"
#include "preparationimage.h"
#include "resolutionfactory.h"
#include "segmentationimage.h"
#include "recognitiontext.h"

//const int SIZE_LETTERS=15;

class RecognitionTest : public QObject
{
    Q_OBJECT

public:
    RecognitionTest();



    void savePaths(QMapIterator<int, QString> &i, const QFile &file);
private Q_SLOTS:
    void testResolution();
    void testResolution_data();
    void testPreparation();
    void testPreparation_data();
    void testSegmentation();
    void testSegmentation_data();
    void testRecognition();
    void testRecognition_data();
private:
    void getAreasSegmentation(const QString &path);
    void repairJson();

    QSharedPointer<OriginalImage> original;
    QSharedPointer<PreparationImage> preparation;
    QSharedPointer<SegmentationImage> segmentation;
    QSharedPointer<RecognitionText> recognition;

    QMap<QSize,QString> m_images;
    QJsonDocument doc;
};

RecognitionTest::RecognitionTest()
{
    QString path = SRCDIR;
    QFile file(path+"/../testData/images.json");
    if (file.open((QIODevice::ReadOnly|QIODevice::Text))) {
        QTextStream in(&file);
        QByteArray all = in.readAll().toLatin1();
        doc = QJsonDocument::fromJson(all);
        //        repairJson();
    }
}


void RecognitionTest::testResolution()
{
    QFETCH(QString, path);
    QFETCH(int, width);
    QFETCH(int, height);

    QFile file(path);
    if (file.exists()) {
        QImage image(path);
        QCOMPARE(image.width(),width);
        QCOMPARE(image.height(),height);
    }
}

void RecognitionTest::testResolution_data()
{
    QTest::addColumn<QString>("path");
    QTest::addColumn<int>("width");
    QTest::addColumn<int>("height");

    QJsonArray arrayObject = doc.array();
    for (int i=0;i<arrayObject.size();i++) {
        QJsonObject object = arrayObject.at(i).toObject();
        QString path = QString(SRCDIR)+"../testData/" + object["path"].toString();
        int width = object["width"].toInt();
        int height = object["height"].toInt();
        QTest::newRow(path.toStdString().c_str()) << path << width << height;
    }

}

bool qMapLessThanKey(const QSize &size1, const QSize &size2) {
    if (size1.width()<size2.width())
        return true;
    else
        if (size1.width()>size2.width())
            return false;
        else
            if (size1.height()<size2.height())
                return true;
    return false;
}

void RecognitionTest::getAreasSegmentation(const QString& path)
{
    QFile file(path);
    if (file.exists()) {
        QFile file(path);
        QFileInfo fileInfo(file);

        QImage image(path);

        QFile output(fileInfo.absolutePath()+"/s_"+fileInfo.baseName()+".json");
        if (!output.open(QIODevice::Text|QIODevice::WriteOnly))
            return;
        QTextStream out(&output);
        QMap<int,QRect> segms;

        ResolutionFactoryImpl factory;
        QSharedPointer<Resolution> resolution = factory.make(image.width(),image.height());


        original.reset(new OriginalImage(image,path));
        preparation = toPreparation(original);
        std::vector<int> columns= resolution->columns();
        cv::Point tl=resolution->tl();
        cv::Point br=resolution->br();
        cv::Rect roi=resolution->rect();
        //        cv::Rect roi=resolution->stationRect();
        cv::Mat small(preparation->getImage(),roi);
        cv::Mat src;
        cv::cvtColor(small, src,cv::COLOR_GRAY2RGBA);

        small =  cv::Scalar::all(255) - small;

        cv::Mat grad;
        cv::Mat morphKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3));
        cv::morphologyEx(small, grad, cv::MORPH_GRADIENT, morphKernel);

        cv::Mat bw;
        cv::threshold(grad, bw, 0.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);

        cv::Mat connected;
        morphKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 1));
        morphologyEx(bw, connected, cv::MORPH_CLOSE, morphKernel);

        cv::Mat mask = cv::Mat::zeros(bw.size(), CV_8UC1);
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

        for(int idx = 0; idx >= 0; idx = hierarchy[idx][0])
        {
            cv::Rect rect = cv::boundingRect(contours[idx]);
            cv::Mat maskROI(mask, rect);
            maskROI = cv::Scalar(0, 0, 0);
            // fill the contour
            cv::drawContours(mask, contours, idx, cv::Scalar(255, 255, 255), CV_FILLED);
            // ratio of non-zero pixels in the filled region
            double r = (double)cv::countNonZero(maskROI)/(rect.width*rect.height);

            if (r > .45 /* assume at least 45% of the area is filled if it contains text */
                    &&
                    (rect.height > 8 && rect.width > 10) /* constraints on region size */
                    /* these two conditions alone are not very robust. better to use something
                                                        like the number of significant peaks in a horizontal projection as a third condition */
                    )
            {
                int col=0;
                int row=rect.y+rect.height/2;
                row=18*(double)row/(br.y-tl.y);
                for (int i=1;i<=(int)columns.size();i++) {
                    int nextColumn;
                    if (i<(int)columns.size())
                        nextColumn=columns.at(i);
                    else
                        nextColumn=br.x-tl.x;
                    if (rect.x>=columns.at(i-1) && rect.x+rect.width<=nextColumn)
                        col=i;
                }



                QRect currentRect = QRect(rect.x,rect.y,rect.width,rect.height);
                if (segms.contains(row*7+col)) {
                    QRect prev=segms.value(row*7+col);
                    currentRect = prev.united(currentRect);
                }
                segms.insert(row*7+col,currentRect);

            }
        }
        QMapIterator<int, QRect> i(segms);
        out<<"[\n";
        while (i.hasNext()) {
            i.next();

            QRect value = i.value();
            cv::Rect rect;
            rect.x=value.x();
            rect.y=value.y();
            rect.width=value.width();
            rect.height=value.height();

            QString text=QString::number(i.key());
            int fontFace = cv::FONT_HERSHEY_SCRIPT_SIMPLEX;
            double fontScale = 1;
            int thickness = 3;
            int baseline=0;
            cv::Size textSize = cv::getTextSize(text.toStdString(), fontFace,
                                                fontScale, thickness, &baseline);
            baseline += thickness;

            // center the text
            cv::Point textOrg(rect.x+(rect.width - textSize.width)/2,
                              rect.y+(rect.height + textSize.height)/2);
            // then put the text itself
            cv::putText(src, text.toStdString(), textOrg, fontFace, fontScale,
                        cv::Scalar(0, 0, 255),thickness);
            cv::rectangle(src, rect, cv::Scalar(0, 255, 0), 1);

            QString data=QString("{\"index\": %1, \"x\": %2, \"y\": %3, \"width\": %4, \"height\": %5}").arg(i.key()).arg(rect.x).arg(rect.y).arg(rect.width).arg(rect.height);
            out<<data<<endl;
        }
        out<<"]\n";
        output.close();

        QImage img((uchar*) src.data, src.cols, src.rows, (int)src.step, QImage::Format_RGB32);
        img.save(fileInfo.absolutePath()+"/_"+fileInfo.baseName()+".png");
    }
}

void RecognitionTest::repairJson()
{
    ResolutionFactoryImpl factory;

    QJsonArray arrayObject = doc.array();
    for (int i=0;i<arrayObject.size();i++) {
        QJsonObject object = arrayObject.at(i).toObject();
        QJsonObject objectSegmentation = object["segmentation"].toObject();
        int width = object["width"].toInt();
        int height = object["height"].toInt();
        QSharedPointer<Resolution> resolution = factory.make(width,height);
        QJsonArray arrayArea = objectSegmentation["area"].toArray();
        cv::Point tl = resolution->tl();
        cv::Point st = resolution->stationRect().tl();
        for (int j=0;j<arrayArea.size();j++) {
            QJsonObject objectArea = arrayArea.at(j).toObject();
            int index = objectArea["index"].toInt();
            int x = objectArea["x"].toInt();
            int y = objectArea["y"].toInt();

            if (index>0) {
                x+= tl.x;
                y+= tl.y;
            } else {
                x+= st.x;
                y+= st.y;
            }
            objectArea["x"] = x;
            objectArea["y"] = y;
            arrayArea[j] = objectArea;
        }
        objectSegmentation["area"]=arrayArea;
        object["segmentation"]=objectSegmentation;
        arrayObject[i]=object;
    }
    doc.setArray(arrayObject);

    QByteArray data = doc.toJson();
    QFile file("ready.json");
    if (file.open(QIODevice::Text|QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out<<data;
        file.close();
    }
}

void RecognitionTest::testPreparation()
{
    QFETCH(QString, path);
    QFETCH(QString, prepared);

    ResolutionFactoryImpl factory;

    QFile fileOriginal(path);
    QFile filePrepared(prepared);
    if (fileOriginal.exists() && filePrepared.exists()) {
        QImage image(path);

        original.reset(new OriginalImage(image,path));
        preparation = toPreparation(original);
        cv::Mat src = preparation->getImage();
        cv::cvtColor(src, src,cv::COLOR_GRAY2RGBA);
        QImage result((uchar*) src.data, src.cols, src.rows, (int)src.step, QImage::Format_RGB32);

        QImage compareImage(prepared);

        QPainter painter(&compareImage);
        painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
        painter.drawImage(0, 0, result);
        painter.end();
        QSharedPointer<Resolution> resolution = factory.make(image.width(),image.height());
        cv::Rect rect = resolution->rect();
        int sum = 0;
        int sumSqr = 0;
        for(int i=rect.tl().x;i<=rect.br().x;i++)
            for(int j=rect.tl().y;j<=rect.br().y;j++) {
                QRgb rgb=compareImage.pixel(i,j);
                int gray = qGray(rgb);
                sum+=gray;
                sumSqr+=std::pow(gray,2);
            }
        int N = rect.size().area();
        double avg = (double)sum / N;
        double avgSqr = (double)sumSqr / N;
        double sigma = std::sqrt(avgSqr-std::pow(avg,2));
        //        qDebug()<<sigma;
        QVERIFY(sigma<=20.0); // quality preparation (~0.0 perfect)

        //        QFile file(path);
        //        QFileInfo fileInfo(file);
        //        compareImage.save(fileInfo.absolutePath()+"/_"+fileInfo.baseName()+".png");
    }
}

void RecognitionTest::testPreparation_data()
{
    QTest::addColumn<QString>("path");
    QTest::addColumn<QString>("prepared");

    QJsonArray arrayObject = doc.array();
    for (int i=0;i<arrayObject.size();i++) {
        QJsonObject object = arrayObject.at(i).toObject();
        QString path = QString(SRCDIR)+"../testData/" + object["path"].toString();
        QJsonObject objectPreparation = object["preparation"].toObject();
        QString pathPreparated = QString(SRCDIR)+"../testData/" + objectPreparation["path"].toString();
        QTest::newRow(path.toStdString().c_str()) << path << pathPreparated;
    }
}

void RecognitionTest::testSegmentation()
{
    QFETCH(QString, path);
    QFETCH(mapRects, rects);

    //    getAreasSegmentation(path);

    QFile file(path);
    if (file.exists()) {
        QImage image(path);
        original.reset(new OriginalImage(image,path));
        preparation = toPreparation(original);
        segmentation = toSegmentation(preparation);
        segmentation->save();
        mapRects rectsSegm = segmentation->getRects();

        QCOMPARE(rectsSegm.size(),rects.size());

        QMapIterator<int, QRect> i(rects);
        while(i.hasNext()) {
            i.next();
            QRect rect = i.value();
            QVERIFY(rectsSegm.contains(i.key()));
            QRect rectSegm = rectsSegm.value(i.key());
            QRect united = rect.united(rectSegm);
            QVERIFY(united.isValid());
        }
    }
}

void RecognitionTest::testSegmentation_data()
{
    QTest::addColumn<QString>("path");
    QTest::addColumn<mapRects>("rects");

    QJsonArray arrayObject = doc.array();
    for (int i=0;i<arrayObject.size();i++) {
        QJsonObject object = arrayObject.at(i).toObject();
        QString path = QString(SRCDIR)+"../testData/" + object["path"].toString();
//        int width = object["width"].toInt();
//        int height = object["height"].toInt();
        QJsonObject objectSegmentation = object["segmentation"].toObject();
        QJsonArray arrayArea = objectSegmentation["area"].toArray();
        mapRects rects;
        for (int j=0;j<arrayArea.size();j++) {
            QJsonObject objectArea = arrayArea.at(j).toObject();
            int index = objectArea["index"].toInt();
            int x = objectArea["x"].toInt();
            int y = objectArea["y"].toInt();
            int width = objectArea["width"].toInt();
            int height = objectArea["height"].toInt();
            QRect rect(x,y,width,height);
            rects.insert(index,rect);
        }
        QTest::newRow(path.toStdString().c_str()) << path <<rects;
    }

}

void RecognitionTest::savePaths(QMapIterator<int, QString>& i, const QFile& file)
{
    QFile output("/Volumes/dane/test1920/recognitions.txt");
    if (!output.open(QIODevice::Append|QIODevice::Text))
        return;
    QTextStream out(&output);
    int column;
    int row;
    if (i.key()==0) {
        column = 0;
        row = 0;
    }
    else {
        column = (i.key()-1)%7+1;
        row = (i.key()-1)/7+1;
    }
    QFileInfo fileInfo(file);
    out<<QString("/Volumes/dane/test1920/1680_%1_%2_%3.png| %4\n").arg(fileInfo.baseName()).arg(row).arg(column).arg(i.value());
}

void RecognitionTest::testRecognition()
{
    QFETCH(QString, path);
    QFETCH(mapTexts, texts);

    QFile file(path);
    if (file.exists()) {
        QImage image(path);
        original.reset(new OriginalImage(image,path));
        preparation = toPreparation(original);
        segmentation = toSegmentation(preparation);

        recognition = toRecognition(segmentation);
        mapTexts recTexts = recognition->getTexts();

        QCOMPARE(recTexts.size(),texts.size());
        QMapIterator<int, QString> i(texts);
        while(i.hasNext()) {
            i.next();
            int lengthImage = segmentation->getLength(i.key());
            int lengthText = i.value().length();
            QCOMPARE(lengthImage,lengthText);

            //            if (lengthImage == lengthText) {
            //                savePaths(i, file);
            //                segmentation->save(i.key());
            //                cv::Mat src = images.value(i.key());
            //                for(int j=0;j<lengthText;j++) {
            //                    cv::Mat img(src, cv::Rect(j*src.rows, 0, src.rows, src.rows));
            //                    cv::cvtColor(img, img,cv::COLOR_GRAY2RGBA);
            //                    QImage img2((uchar*) img.data, img.cols, img.rows, (int)img.step, QImage::Format_RGB32);
            //                    img2.save(QString("%1_%2_%3.png").arg(src.rows).arg(i.value().at(j)).arg(count++));
            //                }
            //            }
            QVERIFY(recTexts.contains(i.key()));
            QString textRec= recTexts.value(i.key());
            QString text = i.value();
            QCOMPARE(textRec,text);
        }
    }
}

void RecognitionTest::testRecognition_data()
{
    QTest::addColumn<QString>("path");
    QTest::addColumn<mapTexts>("texts");

    QJsonArray arrayObject = doc.array();
    for (int i=0;i<arrayObject.size();i++) {
        QJsonObject object = arrayObject.at(i).toObject();
//        int width = object["width"].toInt();
//        int height = object["height"].toInt();
        QString path = QString(SRCDIR)+"../testData/" + object["path"].toString();
        QJsonObject objectSegmentation = object["recognition"].toObject();
        QJsonArray arrayArea = objectSegmentation["area"].toArray();
        mapTexts texts;
        for (int j=0;j<arrayArea.size();j++) {
            QJsonObject objectArea = arrayArea.at(j).toObject();
            int index = objectArea["index"].toInt();
            QString text = objectArea["text"].toString();

            texts.insert(index,text);
        }
        QTest::newRow(path.toStdString().c_str()) << path <<texts;
    }
}


QTEST_MAIN(RecognitionTest)

#include "tst_recognitiontest.moc"
